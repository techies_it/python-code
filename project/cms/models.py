from django.db import models
from ckeditor.fields import RichTextField
from project.account.models import Project

class Cms(models.Model):
    title = models.CharField(max_length=100)
    description = RichTextField()

    def __str__(self):
        return self.title

class Feature(models.Model):
    title = models.CharField(max_length=100)
    banner = models.ImageField(upload_to='banner')
    description = models.TextField()

    def __str__(self):
        return self.title

class Media(models.Model):
    project = models.ForeignKey(Project, related_name='medias', null=True, blank=True)
    image = models.ImageField(upload_to='images', null=True, blank=True)