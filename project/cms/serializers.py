from rest_framework import serializers
from .models import Cms, Feature, Media

class CmsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Cms

        fields = ('id', 'title', 'description')
        read_only_fields = ('id', 'title','description',)


class FeatureSerializer(serializers.ModelSerializer):

    class Meta:
        model = Feature

        fields = ('id', 'title', 'banner','description')
        read_only_fields = ('id', 'title', 'banner', 'description',)


class MediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Media