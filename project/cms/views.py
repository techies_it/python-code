from .models import Cms, Feature
from .serializers import CmsSerializer, FeatureSerializer, MediaSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404
from rest_framework import generics
from rest_framework import permissions

# CMS detail viw for API
class CmsDetail(APIView):
    def get_object(self, pk):
        try:
            return Cms.objects.get(pk=pk)
        except Cms.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        cms = self.get_object(pk)
        serializer = CmsSerializer(cms)
        return Response(serializer.data)


# Feature list view for API
class FeatureList(generics.ListAPIView):
    model = Feature
    serializer_class = FeatureSerializer
    def get_queryset(self):
        queryset = Feature.objects.all()
        
        return queryset


class AddMedia(generics.CreateAPIView):
    serializer_class = MediaSerializer
    def get_permissions(self):
        return (permissions.IsAuthenticated(),)
    
    def create(self, request):
        serializer = MediaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED) 
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
              
