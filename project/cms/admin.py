from django.contrib import admin

from .models import Cms, Feature

admin.site.register(Cms)

admin.site.register(Feature)
