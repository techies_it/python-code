from django.db import models


class AuraMapping(models.Model):
    project_uuid = models.UUIDField()
    source_id = models.IntegerField()
    dest_id = models.IntegerField()
    item_type = models.CharField(max_length=255)


class AuraOtherMapping(models.Model):
    project_uuid = models.UUIDField()
    source_id = models.IntegerField()
    dest_id = models.IntegerField()
    item_type = models.CharField(max_length=255)
