import os
import unicodecsv as csv

from datetime import timedelta

from django.core.management.base import BaseCommand, CommandError

from project.aura.models import AuraOtherMapping
from project.account.models import Project
from project.recruit.models import TestSubject, Session, Agency, Campaign
from project.report.models import IssueSession, Issue


class Command(BaseCommand):
    help = 'Import legacy AURA references data into database'
    reader_cls = (csv.DictReader,)

    def handle(self, *args, **options):
        input_path = os.path.abspath(os.path.join(os.path.os.path.dirname(__file__), '../../../../data/references.csv'))

        if not os.path.exists(input_path):
            raise CommandError('CSV file {0} does not exist'.format(input_path))

        input_file = open(input_path, 'rb')
        reader_class = self.reader_cls[0]
        input_csv = reader_class(input_file, encoding='utf-8-sig')

        self.import_references(input_csv, input_file, input_path)

        input_file.close()



    def import_references(self, input_csv, input_file, input_path):
        """Imports issue reference from the specified CSV file."""

        for v in ('order', 'reference', 'user', 'time', 'project_id'):
            if not (v in input_csv.fieldnames):
                raise CommandError('Input CSV is missing required column "{0}"'.format(v))

        self.stdout.write('references.csv import starting')

        for i, line in enumerate(input_csv):
            error = False
            updated = False
            line_num = i + 2

            for v in ('project_id', 'reference', 'user', 'time'):
                if not line[v].strip():
                    self.stdout.write(self.style.ERROR('The field "{0}" is missing for issue script {1} on line {2}'.format(v, line['project_id'], line_num)))
                    error = True

            # check project exists
            if not error:
                try:
                    project = Project.objects.get(uuid=line['project_id'])
                except Exception as e:
                    self.stdout.write(self.style.ERROR('Project "{0}" does not exists'.format(v, line['project_id'], line_num)))
                    error = True

            # check issue exists
            if not error:
                ref = line['reference'].replace('R', '')
                try:
                    mapping = AuraOtherMapping.objects.filter(source_id=ref, project_uuid=line['project_id'], item_type='ISSUE').first()
                    issue = Issue.objects.get(id=mapping.dest_id)
                except Exception as e:
                    self.stdout.write(self.style.ERROR('Error get AuraOtherMapping for project {1} on line {2}: {3}'.format(v, line['project_id'], line_num, e.message)))
                    error = True

            # check campaign exists
            if not error:
                try:
                    agency = Agency.objects.get(name='The Fore (Recruitment)')
                    campaign = Campaign.objects.get(project=project, agency=agency)
                except Exception as e:
                    self.stdout.write(self.style.ERROR('Error get Campaign for project {1} on line {2}: {3}'.format(v, line['project_id'], line_num, e.message)))
                    error = True

            # check session exists
            if not error:
                try:
                    session = Session.objects.get(campaign=campaign, test_subject_first_name=line['user'])
                except Exception as e:
                    self.stdout.write(self.style.ERROR('Error get session for project {1} on line {2}: {3}'.format(v, line['project_id'], line_num, e.message)))
                    error = True

            # create IssueSession
            if not error:
                try:
                    time = line['time'].split(":")
                    timestamp = session.start_timestamp + timedelta(hours=int(time[0]),minutes=int(time[1]),seconds=int(time[2]))

                    issuesession, created = IssueSession.objects.get_or_create(
                                                session=session,
                                                issue=issue,
                                                elapsed_time=line['time'],
                                                timestamp=timestamp)
                except Exception as e:
                    self.stdout.write(self.style.ERROR('Error create issuesession for project {1} on line {2}: {3}'.format(v, line['project_id'], line_num, e.message)))
                    error = True

            # update timestamp since auto_add_new override it
            if not error:
                issue_session = IssueSession.objects.filter(session=session, issue=issue)
                issue_session.update(timestamp=timestamp)
                updated = created and False or True



            if not error:
                if not updated:
                    self.stdout.write(self.style.SUCCESS('Successfully imported issuesession for project "{1} on line {2}'.format(v, line['project_id'], line_num)))
                else:
                    self.stdout.write(self.style.SUCCESS('Successfully updated issuesession for project "{1} on line {2}'.format(v, line['project_id'], line_num)))
