import os
import unicodecsv as csv

from django.core.management.base import BaseCommand, CommandError

from project.aura.models import AuraMapping
from project.account.models import Project
from project.capacitor.models import Zone, Event, Navigation, Task
from project.recruit.models import TestSubjectProfile


class Command(BaseCommand):
    help = 'Import legacy AURA project_meta data into database'
    reader_cls = (csv.DictReader,)

    def handle(self, *args, **options):
        input_path = os.path.abspath(os.path.join(os.path.os.path.dirname(__file__), '../../../../data/project_meta.csv'))

        if not os.path.exists(input_path):
            raise CommandError('CSV file {0} does not exist'.format(input_path))

        input_file = open(input_path, 'rb')
        reader_class = self.reader_cls[0]
        input_csv = reader_class(input_file, encoding='utf-8-sig')

        self.import_project_meta(input_csv, input_file, input_path)

        input_file.close()



    def import_project_meta(self, input_csv, input_file, input_path):
        """Imports project_meta from the specified CSV file."""

        for v in ('order','item_type','item_data','item_data2','item_data3','item_data4','project_id'):
            if not (v in input_csv.fieldnames):
                raise CommandError('Input CSV is missing required column "{0}"'.format(v))

        self.stdout.write('project_meta.csv import starting')

        for i, line in enumerate(input_csv):
            error = False
            line_num = i + 2

            for v in ('project_id', 'item_type', 'item_data'):
                if not line[v].strip():
                    self.stdout.write(self.style.ERROR('The field "{0}" is missing for project {1} on line {2}'.format(v, line['project_id'], line_num)))
                    error = True

            # check project exists
            if not error:
                project = Project.objects.get(uuid=line['project_id'])
                if not project:
                    self.stdout.write(self.style.ERROR('Project "{0}" does not exists'.format(v, line['project_id'], line_num)))
                    error = True

            if not error:

                if line['item_type'] == 'ZONE':
                    zone, created = Zone.objects.get_or_create(name=line['item_data'], project_id=project.id)
                    if not zone:
                        self.stdout.write(self.style.ERROR('Error creating zone for project "{1}" on line {2}'.format(v, line['project_id'], line_num)))
                        error = True


                if line['item_type'] == 'COLOURS':
                    profile, created = TestSubjectProfile.objects.get_or_create(name=line['item_data'], \
                                                                             color=line['item_data2'] ,\
                                                                             project_id=project.id)
                    if not profile:
                        self.stdout.write(self.style.ERROR('Error creating colour for project "{1}" on line {2}'.format(v, line['project_id'], line_num)))
                        error = True


                if line['item_type'] == 'NAVIGATION':
                    nav, created = Navigation.objects.get_or_create(name=line['item_data'], project_id=project.id)
                    if not nav:
                        self.stdout.write(self.style.ERROR('Error creating navigation for project "{1}" on line {2}'.format(v, line['project_id'], line_num)))
                        error = True


                if line['item_type'] == 'BEHAVIOUR':
                    event, created = Event.objects.get_or_create(name=line['item_data'], project_id=project.id)
                    if not event:
                        self.stdout.write(self.style.ERROR('Error creating event for project "{1}" on line {2}'.format(v, line['project_id'], line_num)))
                        error = True


                if line['item_type'] == 'TASK':
                    start_occur = None
                    end_occur = None

                    if line['item_data2'] != '':
                        line['item_data2'] = line['item_data2'].split(".")[0]
                        mapping = AuraMapping.objects.get(project_uuid=project.uuid, source_id=line['item_data2'])
                        if mapping:
                            start_occur = mapping.item_type + '_' + str(mapping.dest_id)

                    if line['item_data3'] != '':
                        line['item_data3'] = line['item_data3'].split(".")[0]
                        mapping = AuraMapping.objects.get(project_uuid=project.uuid, source_id=line['item_data3'])
                        if mapping:
                            end_occur = mapping.item_type + '_' + str(mapping.dest_id)

                    task, created = Task.objects.get_or_create(name=line['item_data'], project_id=project.id, \
                                                              start_occur=start_occur, end_occur=end_occur, \
                                                              start_mode=line['item_data4'], end_mode=line['item_data4'])
                    if not task:
                        self.stdout.write(self.style.ERROR('Error creating task for project "{1}" on line {2}'.format(v, line['project_id'], line_num)))
                        error = True


            if not error:
                self.stdout.write(self.style.SUCCESS('Successfully imported project meta "{1}" for project "{2} on line {3}'.format(v, line['item_type'], line['project_id'], line_num)))