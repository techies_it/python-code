import os
import unicodecsv as csv

from django.core.management.base import BaseCommand, CommandError

from project.aura.management.commands.aura_import_client import Command as client_register
from project.aura.management.commands.aura_import_script import Command as scripts
from project.aura.management.commands.aura_import_project import Command as project_meta
from project.aura.management.commands.aura_import_user import Command as users
from project.aura.management.commands.aura_import_other import Command as otherdata
from project.aura.management.commands.aura_import_issue import Command as issues
from project.aura.management.commands.aura_import_reference import Command as references


class Command(BaseCommand):
    help = 'Import legacy AURA data into database'
    reader_cls = (csv.DictReader,)

    def handle(self, *args, **options):
        data_path = os.path.abspath(os.path.join(os.path.os.path.dirname(__file__), '../../../../data'))

        if not os.path.exists(data_path):
            raise CommandError('Data directory {0} does not exist'.format(data_path))

        csv_files = ['client_register',
                     'scripts',
#                     'project_meta',
                     'users',
                     'otherdata',
                     'issues',
                     'references'
                    ]

        for filename in csv_files:
            input_path = os.path.join(data_path, filename + '.csv')
            if not os.path.exists(input_path):
                #self.stderr.write(self.style.ERROR('%s not found.' % data_file))
                raise CommandError('{0} does not exist'.format(input_path))

            input_file = open(input_path, 'rb')
            reader_class = self.reader_cls[0]
            input_csv = reader_class(input_file, encoding='utf-8-sig')
            callback = filename + '.import_' + filename

            eval(callback)(eval(filename)(), input_csv, input_file, input_path)

            input_file.close()