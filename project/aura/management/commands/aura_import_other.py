#
# Remember to convert encoding
# iconv -f "windows-1252" -t "UTF-8" scripts.csv > scripts-new.csv
#

import os
import unicodecsv as csv
#import csv

from datetime import datetime, timedelta
from django.utils.dateparse import parse_datetime

from django.core.management.base import BaseCommand, CommandError

from project.aura.models import AuraMapping, AuraOtherMapping
from project.account.models import Project
from project.recruit.models import Agency, TestSubject, Session, Campaign
from project.capacitor.models import Question, Navigation, Event, Task, StepItem, Step, QuestionOption
from project.report.models import QuestionResult, QuestionOptionResult, EventResult, QuoteComment, TaskResult


class Command(BaseCommand):
    help = 'Import legacy AURA other data into database'
    reader_cls = (csv.DictReader,)

    def handle(self, *args, **options):
        input_path = os.path.abspath(os.path.join(os.path.os.path.dirname(__file__), '../../../../data/otherdata.csv'))

        if not os.path.exists(input_path):
            raise CommandError('CSV file {0} does not exist'.format(input_path))

        input_file = open(input_path, 'rb')
        reader_class = self.reader_cls[0]
        input_csv = reader_class(input_file, encoding='utf-8-sig')
        #input_csv = reader_class(input_file)

        self.import_otherdata(input_csv, input_file, input_path)

        input_file.close()



    def import_otherdata(self, input_csv, input_file, input_path):
        """Imports scripts from the specified CSV file."""

        for v in ('order','data_type','user','response_a','time','parent_id','response_b','project_id'):
            if not (v in input_csv.fieldnames):
                raise CommandError('Input CSV is missing required column "{0}"'.format(v))

        self.stdout.write('otherdata.csv import starting')

        last_script_id = None
        for i, line in enumerate(input_csv):
            error = False
            updated = False
            line_num = i + 2

            for v in ('project_id', 'data_type'):
                if not line[v].strip():
                    self.stdout.write(self.style.ERROR('The field "{0}" is missing for project script {1} on line {2}'.format(v, line['project_id'], line_num)))
                    error = True

            # check project exists
            if not error:
                project = Project.objects.get(uuid=line['project_id'])
                if not project:
                    self.stdout.write(self.style.ERROR('Project "{0}" does not exists'.format(v, line['project_id'], line_num)))
                    error = True

            # check test subject exists
            if not error:
                agency = Agency.objects.get(name='The Fore (Recruitment)')
#                test_subject, created = TestSubject.objects.get_or_create(first_name=line['user'], last_name=project.name, agency_id=agency.id)
#                if not test_subject:
#                    self.stdout.write(self.style.ERROR('Error creating tester "{0}" for project {1} on line {2}'.format(v, line['project_id'], line_num)))
#                    error = True

            # check campaign exists
            if not error:
                campaign = Campaign.objects.get(agency=agency, project=project)
                if not campaign:
                    self.stdout.write(self.style.ERROR('Error getting campaign "{0}" for project {1} on line {2}'.format(v, line['project_id'], line_num)))
                    error = True


            #check session exists
            if not error:
                session = Session.objects.filter(campaign=campaign, test_subject_first_name=line['user']).first()
                if not session:
                    self.stdout.write(self.style.ERROR('Error getting session "{0}" for project {1} on line {2}'.format(v, line['project_id'], line_num)))
                    error = True

            if not error:

                # Question Result
                if line['data_type'] == 'QUESTRESP':
                    if line['response_a'] or line['response_b']:
                        try:
                            line['parent_id'] = line['parent_id'].split(".")[0]
                            mapping = AuraMapping.objects.get(project_uuid=line['project_id'], source_id=line['parent_id'])
                            step_item = StepItem.objects.get(id=mapping.dest_id)
                            response = line['response_b'] + ' ' + line['response_a']
                            time = line['time'].split(":")
                            timestamp = session.start_timestamp + timedelta(hours=int(time[0]),minutes=int(time[1]),seconds=int(time[2]))
                            last_script_id = step_item.step.script.id
                            result, created = QuestionResult.objects.get_or_create(session=session,
                                                                                   question=step_item.question,
                                                                                   response=response.strip(),
                                                                                   elapsed_time=line['time'])
                            if not result:
                                self.stdout.write(self.style.ERROR('Error create result question result for project {0} on line {1}'.format(line['project_id'], line_num)))
                                error = True
                            else:
                                QuestionResult.objects.filter(id=result.id).update(timestamp=timestamp)
                            updated = created and False or True

                        except Exception as e:
                            self.stdout.write(self.style.ERROR('Error create result quest_res for project {0} on line {1}: {2}'.format(line['project_id'], line_num, e.message)))
                            error = True



                # Navigation Result
                if line['data_type'] == 'NAV':
                    navigation = Navigation.objects.filter(project_id=project.id, name=line['response_a']).first()
                    if not navigation:
                        # create project nav item
                        order = Navigation.objects.filter(project_id=project.id).count()
                        navigation = Navigation.objects.create(project_id=project.id, name=line['response_a'], order=order)

                    # check script level nav question exists
                    # get first step last item to see if it is nav question
                    step = Step.objects.get(script_id=last_script_id, order=0)
                    step_items = StepItem.objects.filter(step_id=step.id).all()
                    question = None
                    for s in step_items:
                        if s.type_name() == 'question' and s.question.option_type == 'navigation':
                            question = s.question

                    if not question:
                        q = 'Please talk me through the main menu on the left. Without clicking on anything, please tell me what you\'d expect from each option.'
                        order = StepItem.objects.filter(step_id=step.id).count()
                        step_item = StepItem.objects.create(step_id=step.id, order=order)
                        question = Question.objects.create(step_item=step_item, is_multi=True, name=q, option_type='navigation')

                    option = QuestionOption.objects.filter(name=line['response_a'],question=question).first()
                    if not option:
                        order = QuestionOption.objects.filter(question=question).count()
                        option = QuestionOption.objects.create(name=line['response_a'],question=question,order=order)

                    response = line['response_b']
                    time = line['time'].split(":")
                    timestamp = session.start_timestamp + timedelta(hours=int(time[0]),minutes=int(time[1]),seconds=int(time[2]))
                    result, created = QuestionOptionResult.objects.get_or_create(session=session,
                                                                           option=option,
                                                                           response=response.strip(),
                                                                           elapsed_time=line['time'])
                    if not result:
                        self.stdout.write(self.style.ERROR('Error create result "{0}" for project {1} on line {2}'.format(v, line['project_id'], line_num)))
                        error = True
                    else:
                        QuestionOptionResult.objects.filter(id=result.id).update(timestamp=timestamp)

                    updated = not created

                # Event Result
                if line['data_type'] == 'BEHAVE':
                    event = Event.objects.filter(project_id=project.id, name=line['response_a']).first()
                    if not event:
                        order = Event.objects.filter(project_id=project.id).count()
                        event = Event.objects.create(project_id=project.id, name=line['response_a'], order=order)
                    time = line['time'].split(":")
                    timestamp = session.start_timestamp + timedelta(hours=int(time[0]),minutes=int(time[1]),seconds=int(time[2]))
                    result, created = EventResult.objects.get_or_create(session=session, event=event, elapsed_time=line['time'])
                    if not result:
                        self.stdout.write(self.style.ERROR('Error create result "{0}" for project {1} on line {2}'.format(v, line['project_id'], line_num)))
                        error = True
                    else:
                        EventResult.objects.filter(id=result.id).update(timestamp=timestamp)
                    updated = not created


                # QuoteComment
                if line['data_type'] == 'QUOTE' or line['data_type'] == 'COMMENT':
                    data_type = line['data_type'].capitalize()
                    result, created = QuoteComment.objects.get_or_create(
                                          session=session,
                                          content=line['response_a'],
                                          type=data_type,
                                          elapsed_time='00:00')

                    updated = created and True or False


                # EventResult
                if line['data_type'] == 'TASKEND':
                    # check script level task exists
                    # get first step to see if it is task exists
                    step = Step.objects.get(script_id=last_script_id, order=0)
                    step_items = StepItem.objects.filter(step_id=step.id).all()
                    task = None
                    for s in step_items:
                        if s.type_name() == 'task' and s.task.content == line['response_a']:
                            task = s.task
                    if not task:
                        order = StepItem.objects.filter(step_id=step.id).count()
                        step_item = StepItem.objects.create(step_id=step.id, order=order)
                        task = Task.objects.create(step_item=step_item, content=line['response_a'])

                    response = line['response_b']
                    time = line['time'].split(":")
                    elapsed = line['parent_id'].split(".")[0]
                    timestamp = session.start_timestamp + timedelta(hours=int(time[0]),minutes=int(time[1]),seconds=int(time[2]) - int(elapsed))
                    result, created = TaskResult.objects.get_or_create(session=session,
                                                                           task=task,
                                                                           status=line['response_b'],
                                                                           elapsed_time=line['time'],
                                                                           used_time=line['parent_id'])
                    if not result:
                        self.stdout.write(self.style.ERROR('Error create result "{0}" for project {1} on line {2}'.format(v, line['project_id'], line_num)))
                        error = True
                    else:
                        TaskResult.objects.filter(id=result.id).update(timestamp=timestamp)

                    updated = not created


            if not error:
                if not updated:
                    self.stdout.write(self.style.SUCCESS('Successfully imported other data "{1}" for project "{2} on line {3}'.format(v, line['data_type'], line['project_id'], line_num)))
                else:
                    self.stdout.write(self.style.SUCCESS('Successfully updated other data "{1}" for project "{2} on line {3}'.format(v, line['data_type'], line['project_id'], line_num)))