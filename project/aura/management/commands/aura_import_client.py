import os
import unicodecsv as csv

from django.utils.dateparse import parse_datetime

from django.core.management.base import BaseCommand, CommandError

from project.account.models import Account, Client, Interface, Project


class Command(BaseCommand):
    help = 'Import legacy AURA client_register data into database'
    reader_cls = (csv.DictReader,)

    def handle(self, *args, **options):
        input_path = os.path.abspath(os.path.join(os.path.os.path.dirname(__file__), '../../../../data/client_register.csv'))

        if not os.path.exists(input_path):
            raise CommandError('CSV file {0} does not exist'.format(input_path))

        input_file = open(input_path, 'rb')
        reader_class = self.reader_cls[0]
        input_csv = reader_class(input_file, encoding='utf-8-sig')

        self.import_client_register(input_csv, input_file, input_path)

        input_file.close()



    def import_client_register(self, input_csv, input_file, input_path):
        """Imports client project from the specified CSV file."""

        for v in ('project_id', 'client', 'interface', 'project'):
            if not (v in input_csv.fieldnames):
                raise CommandError('Input CSV is missing required column "{0}"'.format(v))

        self.stdout.write('client_register.csv import starting')

        for i, line in enumerate(input_csv):
            error = False
            line_num = i + 2

            for v in ('project_id', 'client', 'interface', 'project'):
                if not line[v].strip():
                    self.stdout.write(self.style.ERROR('The field "{0}" is missing for project {1} on line {2}'.format(v, line['project_id'], line_num)))
                    error = True

            if not error:
                account, created = Account.objects.get_or_create(name='The Fore', owner_id=2, license='100')
                if not account:
                    self.stdout.write(self.style.ERROR('Account "{0}" creation failed'.format(v, line['project_id'], line_num)))
                    error = True

            if not error:
                client, created = Client.objects.get_or_create(name=line['client'], account_id=account.id)
                if not client:
                    self.stdout.write(self.style.ERROR('Client "{0}" creation failed'.format(v, line['project_id'], line_num)))
                    error = True

            if not error:
                interface, created = Interface.objects.get_or_create(name=line['interface'], client_id=client.id)
                if not interface:
                    self.stdout.write(self.style.ERROR('Interface "{0}" creation failed'.format(v, line['project_id'], line_num)))
                    error = True

            if not error:
                project, created = Project.objects.get_or_create(uuid=line['project_id'], name=line['project'], interface_id=interface.id, created_by_id=1)
                if not project:
                    self.stdout.write(self.style.ERROR('Project "{0}" creation failed'.format(v, line['project_id'], line_num)))
                    error = True
                elif not created:
                    self.stdout.write(self.style.WARNING('Skipping already imported client registry "%s"' % line['project_id']))
                    error = True

                start = parse_datetime('2016-07-01T09:00:00+10')
                Project.objects.filter(id=project.id).update(created_at=start, updated_at=start)

            if not error:
                self.stdout.write(self.style.SUCCESS('Successfully imported client registry "%s"' % line['project_id']))