#
# Remember to convert encoding
# iconv -f "windows-1252" -t "UTF-8" scripts.csv > scripts-new.csv
#

import os
import unicodecsv as csv
#import csv

from django.core.management.base import BaseCommand, CommandError

from django.db.models import Q
from django.db.models.functions import Concat
from django.db.models.expressions import Value


from project.aura.models import AuraMapping
from project.account.models import Project
from project.capacitor.models import Script, Step, Question, QuestionOption, Zone, Instruction, Note, StepItem


class Command(BaseCommand):
    help = 'Import legacy AURA script data into database'
    reader_cls = (csv.DictReader,)

    def handle(self, *args, **options):
        input_path = os.path.abspath(os.path.join(os.path.os.path.dirname(__file__), '../../../../data/scripts.csv'))

        if not os.path.exists(input_path):
            raise CommandError('CSV file {0} does not exist'.format(input_path))

        input_file = open(input_path, 'rb')
        reader_class = self.reader_cls[0]
        input_csv = reader_class(input_file, encoding='utf-8-sig')
        #input_csv = reader_class(input_file)

        self.import_scripts(input_csv, input_file, input_path)

        input_file.close()



    def import_scripts(self, input_csv, input_file, input_path):
        """Imports scripts from the specified CSV file."""

        scripts = []
        steps = []
        questions = []

        for v in ('order','type','item_id','parent_id','item_data','step_default_zone','project_id'):
            if not (v in input_csv.fieldnames):
                raise CommandError('Input CSV is missing required column "{0}"'.format(v))

        self.stdout.write('scripts.csv import starting')

        current_project_id = ''
        last_item_id = 0;
        for i, line in enumerate(input_csv):
            error = False
            line_num = i + 2

            for v in ('project_id', 'type'):
                if not line[v].strip():
                    self.stdout.write(self.style.ERROR('The field "{0}" is missing for project script {1} on line {2}'.format(v, line['project_id'], line_num)))
                    error = True

            # reset variable for new project
            if not error and current_project_id != line['project_id']:
                current_project_id = line['project_id']
                current_script = None
                id_mapping = {}
                steps_per_script = {}      # count number of steps
                items_per_step = {}      # count number of item in steps
                options_per_question = {} # count number of options
                zones_per_project = 0 # count number of zones


            # check project exists
            if not error:
                project = Project.objects.get(uuid=line['project_id'])
                if not project:
                    self.stdout.write(self.style.ERROR('Project "{0}" does not exists'.format(v, line['project_id'], line_num)))
                    error = True

            if not error:

                # make sure parent_id is not decimal
                line['parent_id'] = line['parent_id'].split(".")[0]


                # handle script
                if line['type'] == 'SCRIPT':
                    script, created = Script.objects.get_or_create(name=line['item_data'], project_id=project.id)
                    if not script:
                        self.stdout.write(self.style.ERROR('Error creating script for project "{0}"'.format(v, line['project_id'], line_num)))
                        error = True
                    else:
                        AuraMapping.objects.update_or_create(project_uuid=line['project_id'], source_id=line['item_id'], dest_id=script.id, item_type='script')
                        id_mapping[line['item_id']] = script.id
                        current_script = script.id
                        steps_per_script[line['item_id']] = 0


                # handle step
                if line['type'] == 'STEP':
                    if line['step_default_zone'] != '':
                        zone_name = line['step_default_zone'].replace('Z_', '')
                        zone = Zone.objects.filter(name=zone_name, project_id=project.id).first()
                        if not zone:
                            zone = Zone.objects.create(name=zone_name, project_id=project.id, order=zones_per_project)
                            zones_per_project += 1

                    step, created = Step.objects.get_or_create(name=line['item_data'], default_zone_id=zone.id,\
                                                               order=steps_per_script[line['parent_id']], \
                                                               script_id=id_mapping[line['parent_id']])
                    if not step:
                        self.stdout.write(self.style.ERROR('Error creating step for project "{0}"'.format(v, line['project_id'], line_num)))
                        error = True
                    else:
                        AuraMapping.objects.update_or_create(project_uuid=line['project_id'], source_id=line['item_id'], dest_id=step.id, item_type='step')
                        id_mapping[line['item_id']] = step.id
                        steps_per_script[line['parent_id']] += 1
                        items_per_step[line['item_id']] = 0


                # handle instruction
                if line['type'] == 'INST':
                    if line['parent_id'] != '':
                        # check if last item is instruction, if yes, combine.
                        ####last_item_id = int(line['item_id']) - 1
                        last_mapping = AuraMapping.objects.filter(project_uuid=line['project_id'], source_id=last_item_id, item_type='instruction')
                        if last_mapping:
                            instruction = Instruction.objects.filter(step_item__id=last_mapping[0].dest_id).exclude(Q(content__contains=line['item_data']))
                            if instruction:
                                AuraMapping.objects.create(project_uuid=line['project_id'], source_id=line['item_id'], dest_id=instruction[0].step_item.id, item_type='instruction')
                                content = '\n' + line['item_data']
                                instruction.update(content=Concat('content', Value(content)))
                        else:
                            mapping = AuraMapping.objects.filter(project_uuid=line['project_id'], source_id=line['item_id'], item_type='instruction')
                            if not mapping:
                                step_item = StepItem.objects.create(step_id=id_mapping[line['parent_id']], order=items_per_step[line['parent_id']])
                                instruction = Instruction.objects.create(step_item=step_item, content=line['item_data'])
                                AuraMapping.objects.create(project_uuid=line['project_id'], source_id=line['item_id'], dest_id=instruction.step_item.id, item_type='instruction')
                                items_per_step[line['parent_id']] += 1
                    else:
                        # for INST without parent_id, we assume it is script scenario
                        Script.objects.filter(id=current_script).update(scenario=line['item_data'])


                # handle note
                if line['type'] == 'NOTE':
                    # check if last item is note, if yes, combine.
                    ####last_item_id = int(line['item_id']) - 1
                    last_mapping = AuraMapping.objects.get(project_uuid=line['project_id'], source_id=last_item_id)
                    if last_mapping and last_mapping.item_type=='note':
                        ##note = Note.objects.filter(step_item__id=last_mapping.dest_id).exclude(Q(content__contains=line['item_data']))
                        mapping, created  = AuraMapping.objects.get_or_create(project_uuid=line['project_id'], source_id=line['item_id'], dest_id=last_mapping.dest_id, item_type='note')
                        if not mapping:
                            content = '\n' + line['item_data']
                            note.update(content=Concat('content', Value(content)))
                    else:
                        mapping = AuraMapping.objects.filter(project_uuid=line['project_id'], source_id=line['item_id'], item_type='note')
                        if not mapping:
                            step_item = StepItem.objects.create(step_id=id_mapping[line['parent_id']], order=items_per_step[line['parent_id']])
                            note = Note.objects.create(step_item=step_item, content=line['item_data'])
                            AuraMapping.objects.create(project_uuid=line['project_id'], source_id=line['item_id'], dest_id=note.step_item.id, item_type='note')
                            items_per_step[line['parent_id']] += 1


                # handle question
                if line['type'] == 'QUEST':
                    mapping = AuraMapping.objects.filter(project_uuid=line['project_id'], source_id=line['item_id'], item_type='question')
                    if not mapping:
                        step_item = StepItem.objects.create(step_id=id_mapping[line['parent_id']], order=items_per_step[line['parent_id']])
                        question = Question.objects.create(name=line['item_data'], step_item=step_item)
                        AuraMapping.objects.create(project_uuid=line['project_id'], source_id=line['item_id'], dest_id=question.step_item.id, item_type='question')
                        id_mapping[line['item_id']] = step_item.id
                    else:
                        id_mapping[line['item_id']] = mapping.first().dest_id
                    items_per_step[line['parent_id']] += 1
                    options_per_question[line['item_id']] = 0

                # handle question note
                if line['type'] == 'QUESTNOTE':
                    Question.objects.filter(step_item__id=id_mapping[line['parent_id']]).update(note=line['item_data'])

                # handle question option
                if line['type'] == 'QUESTRESP':
                    Question.objects.filter(step_item_id=id_mapping[line['parent_id']]).update(option_type='custom')
                    question = Question.objects.get(step_item_id=id_mapping[line['parent_id']])
                    option, created = QuestionOption.objects.get_or_create(name=line['item_data'],
                                                                  order=options_per_question[line['parent_id']],
                                                                  question=question)
                    if not option:
                        self.stdout.write(self.style.ERROR('Error creating question option for project "{0}"'.format(v, line['project_id'], line_num)))
                        error = True
                    else:
                        id_mapping[line['item_id']] = option.id
                        options_per_question[line['parent_id']] += 1


            if not error:
                self.stdout.write(self.style.SUCCESS('Successfully imported script data "{1}" for project "{2} on line {3}'.format(v, line['type'], line['project_id'], line_num)))


            # update last_item id
            last_item_id = int(line['item_id'])