#
# Remember to convert encoding
# iconv -f "windows-1252" -t "UTF-8" scripts.csv > scripts-new.csv
#

import os
import unicodecsv as csv
#import csv

from django.core.management.base import BaseCommand, CommandError

from project.aura.models import AuraOtherMapping
from project.account.models import Project
from project.report.models import Issue, Zone


class Command(BaseCommand):
    help = 'Import legacy AURA script data into database'
    reader_cls = (csv.DictReader,)

    def handle(self, *args, **options):
        input_path = os.path.abspath(os.path.join(os.path.os.path.dirname(__file__), '../../../../data/issues.csv'))

        if not os.path.exists(input_path):
            raise CommandError('CSV file {0} does not exist'.format(input_path))

        input_file = open(input_path, 'rb')
        reader_class = self.reader_cls[0]
        input_csv = reader_class(input_file, encoding='utf-8-sig')
        #input_csv = reader_class(input_file)

        self.import_issues(input_csv, input_file, input_path)

        input_file.close()



    def import_issues(self, input_csv, input_file, input_path):
        """Imports scripts from the specified CSV file."""

        for v in ('order','zone','priority','title','description','solution','category','project_id'):
            if not (v in input_csv.fieldnames):
                raise CommandError('Input CSV is missing required column "{0}"'.format(v))

        self.stdout.write('issues.csv import starting')

        for i, line in enumerate(input_csv):
            error = False
            line_num = i + 2

            for v in ('project_id', 'zone', 'title'):
                if not line[v].strip():
                    self.stdout.write(self.style.ERROR('The field "{0}" is missing for issue script {1} on line {2}'.format(v, line['project_id'], line_num)))
                    error = True

            # check project exists
            if not error:
                project = Project.objects.get(uuid=line['project_id'])
                if not project:
                    self.stdout.write(self.style.ERROR('Project "{0}" does not exists'.format(v, line['project_id'], line_num)))
                    error = True

            # get issue zone
            if not error:
                zone = Zone.objects.filter(name=line['zone'], project_id=project.id).first()
                if not zone:
                    order = Zone.objects.filter(name=line['zone'], project_id=project.id).count()
                    zone = Zone.objects.create(name=line['zone'], project_id=project.id, order=order)
#                    self.stdout.write(self.style.ERROR('Error creating zone for issue "{1} on line {2}"'.format(v, line['project_id'], line_num)))
#                    error = True

            if not error:
                try:
                    priority = line['priority'].replace('priority', '')
                    priority = priority.replace('issue', '').strip()
                    if priority == 'Enhancement':
                        priority = None
                    if len(line['title']) > 255:
                        title = line['title'][:250] + '...'
                        content = line['title'] + '\n' + line['description']
                    else:
                        title = line['title']
                        content = line['description']

                    issue, created = Issue.objects.get_or_create(zone=zone, priority=priority, \
                                                                title=title, content=content, \
                                                                solution=line['solution'], category=line['category'])
                    if not issue:
                        self.stdout.write(self.style.ERROR('Error creating issue for project "{1} on line {2}"'.format(v, line['project_id'], line_num)))
                        error = True
                    else:
                        AuraOtherMapping.objects.get_or_create(project_uuid=line['project_id'], source_id=line['reference'], dest_id=issue.id, item_type='ISSUE')

                except Exception as e:
                        self.stdout.write(self.style.ERROR('Error creating issue for project {1} on line {2}: {3}'.format(v, line['project_id'], line_num, e.message)))
                        error = True


            if not error:
                self.stdout.write(self.style.SUCCESS('Successfully imported issue data reference "{1}" for project "{2} on line {3}'.format(v, line['reference'], line['project_id'], line_num)))