import os
import unicodecsv as csv

from django.utils import timezone
from django.utils.dateparse import parse_datetime

from django.core.management.base import BaseCommand, CommandError

from project.aura.models import AuraMapping
from project.account.models import Project
from project.capacitor.models import Script
from project.recruit.models import TestSubject, TestSubjectProfile, Session, Agency, Campaign


class Command(BaseCommand):
    help = 'Import legacy AURA users data into database'
    reader_cls = (csv.DictReader,)

    def handle(self, *args, **options):
        input_path = os.path.abspath(os.path.join(os.path.os.path.dirname(__file__), '../../../../data/users.csv'))

        if not os.path.exists(input_path):
            raise CommandError('CSV file {0} does not exist'.format(input_path))

        input_file = open(input_path, 'rb')
        reader_class = self.reader_cls[0]
        input_csv = reader_class(input_file, encoding='utf-8-sig')

        self.import_users(input_csv, input_file, input_path)

        input_file.close()



    def import_users(self, input_csv, input_file, input_path):
        """Imports client project from the specified CSV file."""

        for v in ('project_id', 'user', 'unused', 'description', 'colour', 'time'):
            if not (v in input_csv.fieldnames):
                raise CommandError('Input CSV is missing required column "{0}"'.format(v))

        self.stdout.write('users.csv import starting')

        for i, line in enumerate(input_csv):
            error = False
            line_num = i + 2

            for v in ('project_id', 'user', 'unused'):
                if not line[v].strip():
                    self.stdout.write(self.style.ERROR('The field "{0}" is missing for issue script {1} on line {2}'.format(v, line['project_id'], line_num)))
                    error = True

            # check project exists
            if not error:
                project = Project.objects.get(uuid=line['project_id'])
                if not project:
                    self.stdout.write(self.style.ERROR('Project "{0}" does not exists'.format(v, line['project_id'], line_num)))
                    error = True

            # get script exists
            if not error:
                mapping = AuraMapping.objects.get(source_id=line['unused'], project_uuid=line['project_id'])
                if not mapping:
                    self.stdout.write(self.style.ERROR('Error importing user for project "{1}" on line {2}: script mapping missing'.format(v, line['project_id'], line_num)))
                script = Script.objects.get(id=mapping.dest_id)
                if not script:
                    self.stdout.write(self.style.ERROR('Error importing user for project "{1}" on line {2}: script missing'.format(v, line['project_id'], line_num)))

            # get test subject profile
            if not error:
                test_subject_profile = TestSubjectProfile.objects.filter(color=line['colour'], project_id=project.id).first()
                if not test_subject_profile:
                    order = TestSubjectProfile.objects.filter(project_id=project.id).count()
                    test_subject_profile = TestSubjectProfile.objects.create(color=line['colour'], name=line['colour'],  project_id=project.id, order=order)
                    #self.stdout.write(self.style.ERROR('Error importing user for project "{1}" on line {2}: profile missing'.format(v, line['project_id'], line_num)))

            # get agency
            if not error:
                agency, created = Agency.objects.get_or_create(name='The Fore (Recruitment)',
                                                                account=project.interface.client.account)
                if not agency:
                    self.stdout.write(self.style.ERROR('Error importing user for project "{1}" on line {2}: agency missing'.format(v, line['project_id'], line_num)))

            # create test subject
#            if not error:
#                test_subject, created = TestSubject.objects.update_or_create(first_name=line['user'], last_name=project.name, agency_id=agency.id)
#                if not test_subject:
#                    self.stdout.write(self.style.ERROR('Error creating test subject for project "{1}" on line {2}: profile missing'.format(v, line['project_id'], line_num)))

            # create campaign
            if not error:
                start = parse_datetime('2016-07-01T09:00:00+10')
                end = parse_datetime('2016-07-01T18:00:00+10')
                campaign, created = Campaign.objects.update_or_create(agency=agency,
                                                                    project=project,
                                                                    location='N/A',
                                                                    start=start,
                                                                    end=end,
                                                                    duration=30,
                                                                    tester_required=10)
                if not campaign:
                    self.stdout.write(self.style.ERROR('Error creating campaign for project "{1}" on line {2}: profile missing'.format(v, line['project_id'], line_num)))

            # create session
            if not error:
                datetime = parse_datetime('2016-07-01T'+line['time']+'+10')
                start = parse_datetime('2016-07-01T'+line['time']+'+10')
                session, created = Session.objects.update_or_create(campaign=campaign, \
                                                                 script=script, \
                                                                 datetime=datetime,\
                                                                 test_subject_first_name=line['user'], \
                                                                 test_subject_notes=line['description'], \
                                                                 test_subject_profile=test_subject_profile, \
                                                                 start_timestamp=start)
                if not session:
                    self.stdout.write(self.style.ERROR('Error creating session for project "{1}" on line {2}'.format(v, line['project_id'], line_num)))
                else:
                    Session.objects.filter(id=session.id).update(end_timestamp=start)


            if not error:
                self.stdout.write(self.style.SUCCESS('Successfully imported user data for project "{1} on line {2}'.format(v, line['project_id'], line_num)))