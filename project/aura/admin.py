from django.contrib import admin

from .models import AuraMapping, AuraOtherMapping

admin.site.register(AuraMapping)
admin.site.register(AuraOtherMapping)
