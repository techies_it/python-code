from django.conf.urls import url, include
from rest_framework_nested import routers

from project.account.views import AccountViewSet, UserViewSet, ProjectViewSet, ClientViewSet, InterfaceViewSet, ExampleProject
from project.account.views import ProjectsList, ClientInterfaceGetOrCreate, ProjectsScriptList, AccountTypesList, AccountDetail
from project.authentication.views import LoginView, LogoutView
from project.capacitor.views import EventList, EventDetail
from project.capacitor.views import NavigationList, NavigationDetail
from project.capacitor.views import TaskList, TaskDetail, ProjectTaskList
from project.capacitor.views import ZoneList, ZoneDetail
from project.recruit.views import AgencyViewSet
from project.recruit.views import CampaignList, CampaignDetail
from project.recruit.views import SessionList, SessionDetail
from project.recruit.views import TestSubjectList, TestSubjectDetail
from project.recruit.views import TestSubjectProfileList, TestSubjectProfileDetail
from project.report.views import EventResultCreate, EventResultDetail
from project.report.views import ProjectIssueList, IssueList, IssueDetail, IssueSessionDetail
from project.report.views import ProjectTagList, ProjectCommentList
from project.report.views import QuoteCommentList, QuoteCommentDetail
from project.report.views import TaskResultList, TaskResultDetail
from project.cms.views import CmsDetail, FeatureList, AddMedia

auth_urls = [
    url(r'^/login/$', LoginView.as_view(), name='login'),
    url(r'^/logout/$', LogoutView.as_view(), name='logout'),
]

account_router = routers.SimpleRouter()
account_router.register(r'accounts', AccountViewSet)

user_router = routers.SimpleRouter()
user_router.register(r'users', UserViewSet)

client_router = routers.SimpleRouter()
client_router.register(r'clients', ClientViewSet)

interface_router = routers.SimpleRouter()
interface_router.register(r'interfaces', InterfaceViewSet)

project_router = routers.SimpleRouter()
project_router.register(r'projects', ProjectViewSet)

agency_router = routers.SimpleRouter()
agency_router.register(r'agencies', AgencyViewSet)

zone_urls = [
    url(r'^/(?P<pk>\d+)/$', ZoneDetail.as_view(), name='zone-detail'),
    url(r'^/$', ZoneList.as_view(), name='zone-list')
]

event_urls = [
    url(r'^/(?P<pk>\d+)/$', EventDetail.as_view(), name='event-detail'),
    url(r'^/$', EventList.as_view(), name='event-list')
]

navigation_urls = [
    url(r'^/(?P<pk>\d+)/$', NavigationDetail.as_view(), name='navigation-detail'),
    url(r'^/$', NavigationList.as_view(), name='navigation-list')
]

test_subject_profile_urls = [
    url(r'^/(?P<pk>\d+)/(?P<new_profile_id>\d+)/$', TestSubjectProfileDetail.as_view(), name='test_subject_profile-assing-and-delete'),
    url(r'^/(?P<pk>\d+)/$', TestSubjectProfileDetail.as_view(), name='test_subject_profile-detail'),
    url(r'^/$', TestSubjectProfileList.as_view(), name='test_subject_profile-list')
]

campaign_urls = [
    url(r'^/(?P<pk>\d+)/$', CampaignDetail.as_view(), name='campaign-detail'),
    url(r'^/$', CampaignList.as_view(), name='campaign-list')
]

session_urls = [
    url(r'^/(?P<pk>\d+)/$', SessionDetail.as_view(), name='session-detail'),
    url(r'^/$', SessionList.as_view(), name='session-list')
]

test_subject_urls = [
    url(r'^/(?P<pk>\d+)/$', TestSubjectDetail.as_view(), name='testsubject-detail'),
    url(r'^/$', TestSubjectList.as_view(), name='testsubject-list')
]

task_urls = [
    url(r'^/(?P<pk>\d+)/$', TaskDetail.as_view(), name='task-detail'),
    url(r'^/$', TaskList.as_view(), name='task-list')
]

project_list_url = [
    url(r'^projects-list$', ProjectsList.as_view(), name='projects-list'),
    url(r'^projects-scripts$', ProjectsScriptList.as_view(), name='projects-scripts')
]

project_task_url = [
    url(r'^projects/(?P<uuid>[^/.]+)/tasks/$', ProjectTaskList.as_view(), name='project-task-list')
]

project_issue_url = [
    url(r'^projects/(?P<uuid>[^/.]+)/issues/$', ProjectIssueList.as_view(), name='project-issue-list')
]

project_tag_url = [
    url(r'^projects/(?P<uuid>[^/.]+)/tags/$', ProjectTagList.as_view(), name='project-tag-list'),
    url(r'^projects/(?P<uuid>[^/.]+)/comments/$', ProjectCommentList.as_view(), name='project-comment-list')
]

issue_urls = [
    url(r'^/$', IssueList.as_view(), name='issue-list'),
    url(r'^/(?P<pk>\d+)/$', IssueDetail.as_view(), name='issue-detail'),
]

issue_session_urls = [
    url(r'^issue-session/add$', IssueSessionDetail.as_view(), name='issue-detail'),
    url(r'^issue-session/(?P<iid>\d+)/(?P<sid>\d+)/$', IssueSessionDetail.as_view(), name='issue-detail-delete'),
]

task_result_urls = [
    url(r'^task-result/$', TaskResultList.as_view(), name='taskresult-list'),
    url(r'^task-result/(?P<pk>\d+)/$', TaskResultDetail.as_view(), name='taskresult-detail'),
]

quote_comment_urls = [
    url(r'^quote-comment/$', QuoteCommentList.as_view(), name='quote-comment-list'),
    url(r'^quote-comment/(?P<pk>\d+)/$', QuoteCommentDetail.as_view(), name='quote-comment-detail'),
    url(r'^event-result/add/$', EventResultCreate.as_view(), name='event-result-add'),
    url(r'^event-result/(?P<pk>\d+)/$', EventResultDetail.as_view(), name='event-result-detail')
]

cms_urls = [
    url(r'^cms/(?P<pk>[0-9]+)/$', CmsDetail.as_view(), name='cms-detail'),
    url(r'^add-media/$', AddMedia.as_view(), name='add-media'),
]

feature_urls = [
    url(r'^feature-list/$', FeatureList.as_view(), name='feature-list'),
]

client_interface_urls = [
    url(r'^client-interface/$', ClientInterfaceGetOrCreate.as_view(), name='client-interface'),
]
project_example_url = [
    url(r'^project/example/$', ExampleProject.as_view(), name='project-issue-list')
]
account_extra_url = [
    url(r'^account-types/$', AccountTypesList.as_view(), name='account-type-list'),
    url(r'^account-detail/$', AccountDetail.as_view(), name='account-detail')
]
urlpatterns = [
    url(r'^auth', include(auth_urls)),
    url(r'^', include(account_router.urls)),
    url(r'^', include(user_router.urls)),
    url(r'^', include(project_router.urls)),
    url(r'^', include(agency_router.urls)),
    url(r'^', include(client_router.urls)),
    url(r'^', include(interface_router.urls)),
    url(r'^', include(project_list_url)),
    url(r'^', include(project_task_url)),
    url(r'^', include(project_issue_url)),
    url(r'^', include(project_tag_url)),
    url(r'^', include(issue_session_urls)),
    url(r'^', include(task_result_urls)),
    url(r'^', include(quote_comment_urls)),
    url(r'^', include(project_example_url)),
    url(r'^', include(account_extra_url)),

    url(r'^campaigns', include(campaign_urls)),
    url(r'^test-subjects', include(test_subject_urls)),
    url(r'^test-subject-profiles', include(test_subject_profile_urls)),
    url(r'^sessions', include(session_urls)),
    url(r'^zones', include(zone_urls)),
    url(r'^events', include(event_urls)),
    url(r'^navigation', include(navigation_urls)),
    url(r'^tasks', include(task_urls)),
    url(r'^issues', include(issue_urls)),
    url(r'^', include(cms_urls)),
    url(r'^', include(feature_urls)),
    url(r'^', include(client_interface_urls)),
]
