import uuid
from datetime import timedelta

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone
from project.account.choices import *

class User(AbstractUser):
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)

    @property
    def display_name(self):
        if self.first_name and self.last_name:
            return '%s %s' % (self.first_name, self.last_name)
        elif self.first_name:
            return '%s' % self.first_name
        else:
            return '%s' % self.email

    @property
    def is_owner(self):
        if self.owned.all():
            return True
        return False

    @property
    def status(self):
        if self.password:
            return self.is_active and 'Active' or 'Disabled'
        else:
            return 'Pending'


class Account(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=255, unique=True)
    owner = models.ForeignKey(User, related_name='owned')
    phone = models.CharField(max_length=255, null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    license = models.IntegerField()
    is_paid = models.BooleanField(default=False, blank=True)
    is_enabled = models.BooleanField(default=False, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    type = models.IntegerField(choices=ACCOUNT_TYPES, default=FREE_ACCOUNT)

    users = models.ManyToManyField(User, related_name='accounts', symmetrical=False, blank=True)

    def __str__(self):
        return '%s' % (self.name)

    def type_display_name(self):
        return self.get_type_display()


class Client(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    account = models.ForeignKey(Account)
    owner = models.ForeignKey(User, related_name='clients', null=True, blank=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return '%s' % (self.name)


class Interface(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    client = models.ForeignKey(Client, related_name='interfaces')
    name = models.CharField(max_length=255)

    def __str__(self):
        return '%s' % (self.name)


class Project(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=True)
    interface = models.ForeignKey(Interface, related_name='projects')
    name = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    primary_contact_name = models.CharField(max_length=255, null=True, blank=True)
    primary_contact_email = models.EmailField(max_length=255, null=True, blank=True)
    primary_contact_phone = models.CharField(max_length=255, null=True, blank=True)
    primary_contact_address = models.TextField(null=True, blank=True)
    report_intro = models.TextField(null=True, blank=True)
    report_header = models.TextField(null=True, blank=True)
    report_footer = models.TextField(null=True, blank=True)
    report_objective = models.TextField(null=True, blank=True)
    report_method = models.TextField(null=True, blank=True)
    report_findings = models.TextField(null=True, blank=True)
    report_qualitative = models.TextField(null=True, blank=True)
    report_conclusion = models.TextField(null=True, blank=True)
    created_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_unstructured_session = models.NullBooleanField(null=True)
    is_single_topic = models.NullBooleanField(null=True)
    use_no_script = models.NullBooleanField(null=True)
    use_single_profile = models.NullBooleanField(null=True)
    track_no_event = models.NullBooleanField(null=True)
    is_temporary = models.NullBooleanField(null=True)
    is_completed = models.NullBooleanField(null=True)
    report_recommendation = models.TextField(null=True, blank=True)
    report_theme = models.TextField(null=True, blank=True)

    def __str__(self):
        return '%s' % (self.name)

    def is_recent(self):
        d = timedelta(days=14)
        if self.updated_at > timezone.now() - d:
            return True

        for campaign in self.campaigns.order_by('id'):
            for session in campaign.sessions.order_by('id'):
                if session.end_timestamp and session.end_timestamp > timezone.now() - d:
                    return True
        return False

    def status(self):
        status = 'Open'
        all_completed = True
        if self.is_unstructured_session:
            if self.is_completed:
                all_completed = True
            else:
                all_completed = False
                for session in self.sessions.order_by('id'):
                    if session.end_timestamp:
                        status = 'In progress'
        else:   
            if self.campaigns.order_by('id'): 
                for campaign in self.campaigns.order_by('id'):
                    for session in campaign.sessions.order_by('id'):
                        if session.end_timestamp:
                            status = 'In progress'
                        else:
                            all_completed = False
            else:
                all_completed = False
                
        if all_completed:
            status = 'Completed'

        # legacy project
        if self.id < 58:
           status = 'Completed'

        return status
