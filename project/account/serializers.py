import logging
from django.contrib.auth import update_session_auth_hash
from django.utils import timezone
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from project.account.models import User, Account, Project, Client, Interface
from django.contrib.auth.hashers import make_password
import base64

logger = logging.getLogger(__name__)


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, allow_blank=True, required=False, min_length=6, max_length=20)
    confirm_password = serializers.CharField(write_only=True, allow_blank=True, required=False)
    display_name = serializers.CharField(read_only=True)
    show_account = serializers.SerializerMethodField(read_only=True)
    is_agency = serializers.SerializerMethodField(read_only=True)
    is_client = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = ('id', 'display_name', 'username', 'first_name', 'last_name', 'email', \
                  'date_joined', 'password', 'confirm_password', 'is_active',
                  'show_account', 'is_agency', 'is_client', 'is_owner', 'status')
        ready_only_fields = ('date_joined')

    def get_display_name(self, obj):
        if obj.first_name and obj.last_name:
            return obj.first_name + ' ' + obj.last_name
        elif obj.first_name:
            return obj.first_name    
        else:
            return obj.email

    def get_show_account(self, obj):
        return obj.is_staff

    def get_is_agency(self, obj):
        return obj.agencies.first() and True or False

    def get_is_client(self, obj):
        return obj.clients.first() and True or False

    def create(self, validated_data):
        logger.debug('In UserSerializer about to create %s' % validated_data)
        return User.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.email = validated_data.get('email', instance.email)
        instance.username = validated_data.get('username', instance.username)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.is_active = validated_data.get('is_active', instance.is_active)

        logger.debug('In UserSerializer about to save instance %s' % instance)
        instance.save()

        password = validated_data.get('password', None)
        confirm_password = validated_data.get('confirm_password', None)

        if password and confirm_password and password == confirm_password:
            instance.set_password(password)
            instance.save()

        update_session_auth_hash(self.context.get('request'), instance)

        return instance


class AccountSerializer(serializers.ModelSerializer):
    owner_id = serializers.SerializerMethodField(read_only=True)
    owner_first_name = serializers.SerializerMethodField(read_only=True)
    owner_last_name = serializers.SerializerMethodField(read_only=True)
    owner_email = serializers.SerializerMethodField(read_only=True)
    status = serializers.SerializerMethodField(read_only=True)
    invite = serializers.BooleanField(required=False)

    class Meta:
        model = Account

        fields = ('id', 'uuid', 'owner', 'owner_first_name', 'name', 'is_paid',
                  'owner_last_name', 'owner_email', 'phone', 'address',
                  'owner_id', 'is_enabled', 'license', 'status', 'users', 'invite', 'type', 'type_display_name')
        read_only_fields = ('id', 'uuid')

    def get_owner_id(self, obj):
        return obj.owner.id

    def get_owner_first_name(self, obj):
        return obj.owner.first_name

    def get_owner_last_name(self, obj):
        return obj.owner.last_name

    def get_owner_email(self, obj):
        return obj.owner.email

    def get_status(self, obj):
        if obj.is_enabled:
            return 'Active'
        elif not obj.is_paid:
            return 'Pending'
        else:
            return 'Disabled'

    def get_invite(self, obj):
        return False

    def create(self, validated_data):
        if 'invite' in validated_data:
            del validated_data['invite']
        return Account.objects.create(**validated_data)


class ClientSerializer(serializers.ModelSerializer):
    account_name = serializers.SerializerMethodField(read_only=True)
    interfaces_name = serializers.SerializerMethodField(read_only=True)
    owner_id = serializers.SerializerMethodField(read_only=True)
    owner_first_name = serializers.SerializerMethodField(read_only=True)
    owner_last_name = serializers.SerializerMethodField(read_only=True)
    owner_email = serializers.SerializerMethodField(read_only=True)
    invite = serializers.BooleanField(required=False)

    class Meta:
        model = Client

        fields = ('id', 'uuid', 'account', 'name', 'account_name', 'interfaces_name',
                  'owner', 'owner_id', 'owner_first_name', 'owner_last_name',
                  'owner_email', 'invite')
        read_only_fields = ('id', 'uuid')

        validators = [
            UniqueTogetherValidator(
                queryset=Client.objects.all(),
                fields=('account', 'name')
            )
        ]

    def get_account_name(self, obj):
        return obj.account.name

    def get_interfaces_name(self, obj):
        interfaces = []
        for interface in obj.interfaces.all():
            interfaces.append({'id': interface.id, 'name': interface.name})
        return interfaces

    def get_owner_id(self, obj):
        return obj.owner and obj.owner.id or None

    def get_owner_first_name(self, obj):
        return obj.owner and obj.owner.first_name or None

    def get_owner_last_name(self, obj):
        return obj.owner and obj.owner.last_name or None

    def get_owner_email(self, obj):
        return obj.owner and obj.owner.email or None

    def get_invite(self, obj):
        return False

    def create(self, validated_data):
        if 'invite' in validated_data:
            del validated_data['invite']
        return Client.objects.create(**validated_data)


class InterfaceSerializer(serializers.ModelSerializer):
    client_name = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Interface

        fields = ('id', 'uuid', 'client', 'name', 'client_name', 'projects')
        read_only_fields = ('id', 'uuid', 'projects')

        validators = [
            UniqueTogetherValidator(
                queryset=Interface.objects.all(),
                fields=('client', 'name')
            )
        ]

    def get_client_name(self, obj):
        return obj.client.name

    def create(self, validated_data):
        return Interface.objects.create(**validated_data)


class ProjectSerializer(serializers.ModelSerializer):
    created_by_id = serializers.SerializerMethodField(read_only=True)
    created_by_name = serializers.SerializerMethodField(read_only=True)
    account_name = serializers.SerializerMethodField(read_only=True)
    account_id = serializers.SerializerMethodField(read_only=True)
    client_name = serializers.SerializerMethodField(read_only=True)
    client = serializers.SerializerMethodField(read_only=True)
    client_uuid = serializers.SerializerMethodField(read_only=True)
    interface_name = serializers.SerializerMethodField(read_only=True)
    zones_name = serializers.SerializerMethodField(read_only=True)
    events_name = serializers.SerializerMethodField(read_only=True)
    navigation_name = serializers.SerializerMethodField(read_only=True)
    testsubjectprofiles_name = serializers.SerializerMethodField(read_only=True)
    campaigns_name = serializers.SerializerMethodField(read_only=True)
    scripts_name = serializers.SerializerMethodField(read_only=True)
    has_report_data = serializers.SerializerMethodField(read_only=True)
    unstructrued_sessions = serializers.SerializerMethodField(read_only=True)
    unstructured_test_available = serializers.SerializerMethodField(read_only=True)
    structured_test_available = serializers.SerializerMethodField(read_only=True)
    project_hash = serializers.SerializerMethodField(read_only=True)
    
    class Meta:
        model = Project

        fields = ('id', 'uuid', 'account_name', 'account_id', 'client_name', 'interface_name',
                  'name', 'description', 'primary_contact_name',
                  'primary_contact_email', 'primary_contact_phone',
                  'primary_contact_address', 'created_by_id', 'created_by_name',
                  'zones_name', 'interface', 'client', 'events_name', 'navigation_name',
                  'testsubjectprofiles_name', 'campaigns_name', 'scripts_name',
                  'has_report_data', 'report_header', 'report_intro', 'report_objective',
                  'report_method', 'report_findings', 'report_qualitative',
                  'report_conclusion', 'report_footer', 'client_uuid', 'is_recent', 'status',
                  'is_unstructured_session','is_single_topic','use_no_script','use_single_profile','track_no_event', 
                  'is_temporary', 'is_completed','unstructrued_sessions', 'unstructured_test_available', 'structured_test_available', 'project_hash', 'report_recommendation', 'report_theme'
                  )
        read_only_fields = ('id', 'uuid',)

    def get_created_by_id(self, obj):
        return obj.created_by and obj.created_by.id or None

    def get_created_by_name(self, obj):
        return obj.created_by and obj.created_by.display_name or None

    def get_account_name(self, obj):
        return obj.interface.client.account.name

    def get_account_id(self, obj):
        return obj.interface.client.account.id

    def get_client_name(self, obj):
        return obj.interface.client.name

    def get_client(self, obj):
        return obj.interface.client.id

    def get_client_uuid(self, obj):
        return obj.interface.client.uuid

    def get_interface_name(self, obj):
        return obj.interface.name

    def get_zones_name(self, obj):
        zones_name = []
        for zone in obj.zones.order_by('order'):
            zones_name.append({'id': zone.id, 'name': zone.name, 'order': zone.order, 'screenshot': zone.screenshot})
        return zones_name

    def get_events_name(self, obj):
        events_name = []
        for event in obj.events.order_by('order'):
            events_name.append({'id': event.id, 'name': event.name, 'order': event.order})
        return events_name

    def get_navigation_name(self, obj):
        navigation_name = []
        for nav in obj.navigation.order_by('order'):
            navigation_name.append({'id': nav.id, 'name': nav.name, 'order': nav.order})
        return navigation_name

    def get_testsubjectprofiles_name(self, obj):
        profiles_name = []
        for profile in obj.test_subject_profiles.order_by('order'):
            scripts = []
            for script in profile.scripts.all():
                scripts.append(script.id)
            if profile.sessions.count():
                has_session = True
            else:
                has_session = False
            profiles_name.append({'id': profile.id, 'name': profile.name,
                                  'color': profile.color, 'order': profile.order,
                                  'scripts': scripts, 'has_session': has_session})

        return profiles_name

    def get_campaigns_name(self, obj):
        campaigns_name = []
        for campaign in obj.campaigns.order_by('id'):
            # calculate total session and success rate
            total_sessions = 0
            total_success_sessions = 0
            total_success_percentage = 0
            for session in campaign.sessions.order_by('id'):
                if session.test_subject_first_name != 'Lunch':
                    total_sessions = total_sessions + 1
                    if session.end_timestamp:
                        total_success_sessions = total_success_sessions + 1
            if total_sessions > 0:
                total_success_percentage = total_success_sessions * 100 / total_sessions
            # End of calculate total session and success rate

            campaigns_name.append({
                'id': campaign.id,
                'location': campaign.location,
                'agency': campaign.agency.id,
                'agency_name': campaign.agency.name,
                'date': timezone.localtime(campaign.start).strftime("%d %b %Y"),
                'total_sessions': total_sessions,
                'total_success_sessions': total_success_sessions,
                'total_success_percentage': total_success_percentage,
                'recruitment_link': campaign.recruitment_link()
                })
        return campaigns_name

    def get_scripts_name(self, obj):
        scripts_name = []
        for script in obj.scripts.order_by('id'):
            scripts_name.append({'id': script.id, 'name': script.name, 'has_data': script.sessions.count()})
        return scripts_name

    def get_has_report_data(self, obj):
        if obj.is_unstructured_session:
            for session in obj.sessions.order_by('id'):
                if session.end_timestamp:
                    return True
        else:        
            for campaign in obj.campaigns.order_by('id'):
                for session in campaign.sessions.order_by('id'):
                    if session.end_timestamp:
                        return True
        return False

    def create(self, validated_data):
        return Project.objects.create(**validated_data)

    def get_unstructrued_sessions(self, obj):
        unstructrued_sessions = []
        if obj.is_unstructured_session:
            for session in obj.sessions.order_by('id').exclude(is_unstructured_dry=True):
                unstructrued_sessions.append({'id':session.id,
                'date': timezone.localtime(session.datetime).strftime("%d %b %Y"),
                'start_timestamp': session.start_timestamp,
                'end_timestamp': session.end_timestamp
                })
        return unstructrued_sessions

    def get_unstructured_test_available(self, obj):
        if obj.status() == 'Completed':
            return False
        elif obj.use_no_script != True and not self.get_scripts_name(obj):  
            return False 
        elif obj.is_unstructured_session and obj.status() !='Completed':
            return True
        return False
    
    def get_structured_test_available(self, obj):
        if obj.status() == 'Completed':
            return False
        elif obj.is_unstructured_session != True and obj.status() !='Completed' and self.get_campaigns_name(obj) and obj.use_no_script == True:
            return True
        elif obj.is_unstructured_session != True and obj.status() !='Completed' and self.get_campaigns_name(obj) and self.get_scripts_name(obj):
            return True
        return False

    def get_project_hash(self, obj):
        # this hash value is used to create public links(report)
        salt = str(obj.uuid)
        return base64.b64encode(str(obj.id) + '|' + salt[:8])   


class RegisterAccountSerializer(serializers.ModelSerializer):
    owner_id = serializers.SerializerMethodField(read_only=True)
    owner_first_name = serializers.SerializerMethodField(read_only=True)
    owner_last_name = serializers.SerializerMethodField(read_only=True)
    owner_email = serializers.SerializerMethodField(read_only=True)
    status = serializers.SerializerMethodField(read_only=True)
    invite = serializers.BooleanField(required=False)

    class Meta:
        model = Account

        fields = ('id', 'uuid', 'owner', 'owner_first_name', 'name', 'is_paid',
                  'owner_last_name', 'owner_email', 'phone', 'address',
                  'owner_id', 'is_enabled', 'license', 'status', 'users', 'invite')
        read_only_fields = ('id', 'uuid')

    owner = UserSerializer()

    def get_owner_id(self, obj):
        return obj.owner.id

    def get_owner_first_name(self, obj):
        return obj.owner.first_name

    def get_owner_last_name(self, obj):
        return obj.owner.last_name

    def get_owner_email(self, obj):
        return obj.owner.email

    def get_status(self, obj):
        if obj.is_enabled:
            return 'Active'
        elif not obj.is_paid:
            return 'Pending'
        else:
            return 'Disabled'

    def get_invite(self, obj):
        return False

    def create(self, validated_data):
        if 'invite' in validated_data:
            del validated_data['invite']
        user_data = validated_data.pop('owner')
        password = make_password(user_data['password'])
        user_data['password'] = password
        user_model = User.objects.create(**user_data)
        # password = make_password(user_data['password'])
        # user_model.set_password(password) 
        user_model.save()
        
        account = Account.objects.create(owner=user_model, **validated_data) 
        account.save()
        account.users = [user_model.id]
        account.save()
        return account


class ProjectListSerializer(serializers.ModelSerializer):
    account_name = serializers.SerializerMethodField(read_only=True)
    client_name = serializers.SerializerMethodField(read_only=True)
    client_uuid = serializers.SerializerMethodField(read_only=True)
    interface_name = serializers.SerializerMethodField(read_only=True)
    campaigns_name = serializers.SerializerMethodField(read_only=True)
    scripts_name = serializers.SerializerMethodField(read_only=True)
    has_report_data = serializers.SerializerMethodField(read_only=True)
    status = serializers.SerializerMethodField(read_only=True)
    unstructured_test_available = serializers.SerializerMethodField(read_only=True)
    structured_test_available = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Project

        fields = ('id', 'uuid', 'is_recent',
                  'name', 'account_name', 'client_name',
                  'client_uuid', 'interface_name',
                  'status', 'has_report_data', 'campaigns_name',
                  'scripts_name', 'updated_at', 'is_unstructured_session', 'is_completed', 'is_temporary',
                  'unstructured_test_available', 'structured_test_available'
                  )
        read_only_fields = ('id', 'uuid',)

    def get_account_name(self, obj):
        return obj.interface.client.account.name

    def get_client_name(self, obj):
        return obj.interface.client.name

    def get_client_uuid(self, obj):
        return obj.interface.client.uuid

    def get_interface_name(self, obj):
        return obj.interface.name
    #  check if project has campaigns, value used in project list
    def get_campaigns_name(self, obj):
        return obj.campaigns.first() and True or False   

    #  check if project has scripts, value used in project list
    def get_scripts_name(self, obj):
        return obj.scripts.first() and True or False   

    def get_has_report_data(self, obj):
        if obj.is_unstructured_session:
            for session in obj.sessions.order_by('id'):
                if session.end_timestamp:
                    return True
        else:        
            for campaign in obj.campaigns.order_by('id'):
                for session in campaign.sessions.order_by('id'):
                    if session.end_timestamp:
                        return True
        return False

    def get_status(self, obj):
        return obj.status()
        
    def get_unstructured_test_available(self, obj):
        if obj.status() == 'Completed':
            return False
        elif obj.use_no_script != True and not self.get_scripts_name(obj):  
            return False 
        elif obj.is_unstructured_session and obj.status() !='Completed':
            return True
        return False
    
    def get_structured_test_available(self, obj):
        if obj.status() == 'Completed':
            return False
        elif obj.is_unstructured_session != True and obj.status() !='Completed' and self.get_campaigns_name(obj) and obj.use_no_script == True:
            return True
        elif obj.is_unstructured_session != True and obj.status() !='Completed' and self.get_campaigns_name(obj) and self.get_scripts_name(obj):
            return True
        return False       