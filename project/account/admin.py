from django.contrib import admin

from .models import Account, User, Client, Interface, Project

admin.site.register(User)
admin.site.register(Account)
admin.site.register(Client)
admin.site.register(Interface)
admin.site.register(Project)
