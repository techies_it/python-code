# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-28 05:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0009_auto_20170915_1939'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='is_single_topic',
            field=models.NullBooleanField(),
        ),
        migrations.AddField(
            model_name='project',
            name='is_unstructured_session',
            field=models.NullBooleanField(),
        ),
        migrations.AddField(
            model_name='project',
            name='track_no_event',
            field=models.NullBooleanField(),
        ),
        migrations.AddField(
            model_name='project',
            name='use_no_script',
            field=models.NullBooleanField(),
        ),
        migrations.AddField(
            model_name='project',
            name='use_single_profile',
            field=models.NullBooleanField(),
        ),
    ]
