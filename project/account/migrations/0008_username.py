from __future__ import unicode_literals

import django.contrib.auth.models
import django.core.validators
import django.db.models.deletion
import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('account', '0007_auto_20170406_1123'),
    ]

    operations = [
        migrations.AlterField(
            model_name='User',
            name='username',
            field=models.CharField(error_messages={'unique': 'A user with that username already exists.'},
                                   help_text='Required. 254 characters or fewer. Letters, digits and @/./+/-/_ only.',
                                   max_length=254, unique=True, validators=[
                    django.core.validators.RegexValidator('^[\\w.@+-]+$',
                                                          'Enter a valid username. This value may contain only letters, numbers and @/./+/-/_ characters.')],
                                   verbose_name='username'),
        ),
    ]
