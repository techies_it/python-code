import logging

from django.conf import settings
from django.contrib import messages
from django.core import signing
from django.core.mail import send_mail
from django.db.models import Q
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import redirect
from django.template import loader
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, DeleteView
from rest_framework import permissions, authentication, viewsets
from rest_framework import status
from rest_framework.decorators import detail_route
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView

try:
    from django.contrib.sites.shortcuts import get_current_site
except ImportError:
    from django.contrib.sites.models import get_current_site

from .models import Account, User, Project, Client, Interface
from .forms import CreateUserForm
from .serializers import AccountSerializer, UserSerializer, ProjectSerializer, ClientSerializer, InterfaceSerializer, RegisterAccountSerializer, ProjectListSerializer
from .permissions import IsAccountOwner, IsUserAccessible, IsProjectAccessible

from project.recruit.models import Agency, Campaign, TestSubjectProfile, Session
from project.capacitor.models import Zone, Event, Navigation, Script

import urllib
import urllib2
import json
from django.core.paginator import Paginator
import base64
from project.account.choices import ACCOUNT_TYPES, ACCOUNT_TYPES_LICENSES
from rest_framework.serializers import ValidationError

logger = logging.getLogger(__name__)


class AccountView(TemplateView):
    template_name = 'index.html'

    @method_decorator(ensure_csrf_cookie)
    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
            logger.debug('Redirecting to / because user is not staff')
            return redirect('/')

        return super(AccountView, self).dispatch(*args, **kwargs)


class AccountViewSet(viewsets.ModelViewSet):
    lookup_field = 'uuid'
    queryset = Account.objects.order_by('name')
    serializer_class = AccountSerializer

    def get_permissions(self):
        return (IsAccountOwner(),)

    def get_queryset(self):
        user = self.request.user
        order_by_param = self.request.query_params.get('ordering', None)
        if order_by_param is not None:
            order_by_string = [order_by_param]
        else:
            order_by_string = ['id']
        if user.is_staff:
            return Account.objects.order_by(*order_by_string)
        else:
            return Account.objects.filter(users=user)

    def perform_create(self, serializer):
        instance = serializer.save()
        return super(AccountViewSet, self).perform_create(serializer)

    def perform_update(self, serializer):
        instance = serializer.save()

        if serializer.is_valid() and serializer.validated_data.get('invite', False):
            # send invite email
            # borrow from password_reset packages
            context = {
                'site': get_current_site(self.request),
                'sender': self.request.user.display_name,
                'token': signing.dumps(instance.owner.pk, salt='password_recovery'),
                'secure': self.request.is_secure(),
            }
            body = loader.render_to_string('password_reset/invite_email.txt', context).strip()
            subject = loader.render_to_string('password_reset/invite_email_subject.txt', context).strip()
            send_mail(subject, body, settings.DEFAULT_FROM_EMAIL, [instance.owner.email])

        return super(AccountViewSet, self).perform_update(serializer)


class UserViewSet(viewsets.ModelViewSet):
    lookup_field = 'id'
    queryset = User.objects.order_by('id')
    serializer_class = UserSerializer

    def get_permissions(self):
        return (permissions.IsAuthenticated(), IsUserAccessible(),)

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            return User.objects.order_by('id')
        if user.accounts:
            user_type = self.request.query_params.get('user_type', None)
            if user_type is not None:
                return User.objects.filter(Q(accounts__in=user.accounts.all())).order_by('id')
            else:
                agencies = Agency.objects.filter(account__in=user.accounts.all())
                clients = Client.objects.filter(account__in=user.accounts.all())
                return User.objects.filter(Q(accounts__in=user.accounts.all()) | Q(agencies__in=agencies) | Q(id=user.id) | Q(clients__in=clients)) \
                    .order_by('id')

    def perform_create(self, serializer):
        user_type = self.request.query_params.get('user_type', None)
        if user_type is not None and user_type == 'account':
            """ Hook to ensure if more user is added under account and license is avialable. """
            if self.request.user.accounts.first():
                account = self.request.user.accounts.first()
                total_user = User.objects.filter(Q(accounts__in=self.request.user.accounts.all())).count()
                if account.license<=total_user:
                    raise ValidationError({'Error':'Maximum user limit reached.'})

            instance = serializer.save()        
            instance.accounts = self.request.user.accounts.all()
            instance.save()
            # send invite email
            # borrow from password_reset packages
            context = {
                'site': get_current_site(self.request),
                'sender': self.request.user.display_name,
                'token': signing.dumps(instance.pk, salt='password_recovery'),
                'secure': self.request.is_secure(),
            }
            body = loader.render_to_string('password_reset/invite_email.txt', context).strip()
            subject = loader.render_to_string('password_reset/invite_email_subject.txt', context).strip()
            send_mail(subject, body, settings.DEFAULT_FROM_EMAIL, [instance.email])
        else:
            instance = serializer.save()

        return super(UserViewSet, self).perform_create(serializer)

    @detail_route(methods=['get'], permission_classes=[permissions.IsAuthenticated, IsUserAccessible])
    def reset_password(self, request, *args, **kwargs):
        instance = User.objects.get(id=kwargs['id'])
        """ Hook to ensure object is can be accessed by request.user. """
        if not self.request.user.is_staff and instance.accounts.first() not in self.request.user.accounts.all():
            raise Http404
        # send reset password email
        # borrow from password_reset packages
        context = {
            'site': get_current_site(self.request),
            'sender': self.request.user.display_name,
            'token': signing.dumps(instance.pk, salt='password_recovery'),
            'secure': self.request.is_secure(),
        }
        body = loader.render_to_string('password_reset/reset_email.txt', context).strip()
        subject = loader.render_to_string('password_reset/reset_email_subject.txt', context).strip()
        send_mail(subject, body, settings.DEFAULT_FROM_EMAIL, [instance.email])
        return Response(None, status=status.HTTP_200_OK)


class ClientViewSet(viewsets.ModelViewSet):
    lookup_field = 'uuid'
    queryset = Client.objects.order_by('name')
    serializer_class = ClientSerializer

    def get_permissions(self):
        return (permissions.IsAuthenticated(),)

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            return Client.objects.order_by('name')
        if user.is_authenticated() and user.accounts.all():
            return Client.objects.filter(account__in=user.accounts.all()).order_by('name')

    def perform_create(self, serializer):
        instance = serializer.save()
        return super(ClientViewSet, self).perform_create(serializer)

    def perform_update(self, serializer):
        instance = serializer.save()
        return super(ClientViewSet, self).perform_update(serializer)


class InterfaceViewSet(viewsets.ModelViewSet):
    lookup_field = 'uuid'
    queryset = Interface.objects.order_by('name')
    serializer_class = InterfaceSerializer

    def get_permissions(self):
        return (permissions.IsAuthenticated(),)

    def get_queryset(self):
        user = self.request.user
        if user.is_staff:
            return Interface.objects.order_by('name')
        else:
            return Interface.objects.filter(client__account__owner=user).order_by('name')

    def perform_create(self, serializer):
        instance = serializer.save()
        return super(InterfaceViewSet, self).perform_create(serializer)


class ProjectView(TemplateView):
    template_name = 'index.html'

    @method_decorator(ensure_csrf_cookie)
    def dispatch(self, *args, **kwargs):
        return super(ProjectView, self).dispatch(*args, **kwargs)


class ProjectViewSet(viewsets.ModelViewSet):
    lookup_field = 'uuid'
    queryset = Project.objects.order_by('name')
    serializer_class = ProjectSerializer
    permission_classes = [permissions.IsAuthenticated, IsProjectAccessible]

    # def get_permissions(self):
    #     return permissions.IsAuthenticated(), IsProjectAccessible()

    def get_queryset(self):
        user = self.request.user
        logger.debug('In ProjectViewSet get_queryset, user=%s' % user)
        queryset = Project.objects.all()
        # Filter the project with scripts, if script_only parms is passesd in url. i.e. products/?script_only=1
        script_only = self.request.query_params.get('script_only', None)
        if script_only is not None:
            queryset = queryset.filter(scripts__id__isnull = False).distinct()

        if user.is_staff:
            logger.debug('User is staff')
            return queryset.order_by('interface__client__account__name', 'interface__client__name', 'interface__name', 'name')

        if user.is_authenticated() and user.clients.all():
            logger.debug('User is not staff, filtering by clients %s' % user.clients.all())
            return queryset.filter(interface__client__in=user.clients.all()) \
                                  .order_by('interface__client__account__name', 'interface__client__name',
                                            'interface__name', 'name')
        if user.is_authenticated() and user.accounts.all():
            logger.debug('User is not staff, filtering by accounts %s' % user.accounts.all())
            return queryset.filter(interface__client__account__in=user.accounts.all()) \
                                  .order_by('interface__client__account__name', 'interface__client__name',
                                            'interface__name', 'name')

    def perform_create(self, serializer):
        logger.debug('In ProjectViewSet create')
        # by default set unstructred steps for project to true
        instance = serializer.save(created_by=self.request.user, is_unstructured_session=True, 
        is_single_topic=True, use_no_script=True, use_single_profile=True, track_no_event=True)

         # create default zone for unstructured
        for idx, zone_name in enumerate(settings.DEFAULT_ZONE_UNSTRUCTURED):
            zone = Zone(name=zone_name, project_id=instance.id, order=idx)
            zone.save()
            
        # create default profile
        for idx, persona in enumerate(settings.DEFAULT_PROFILE):
            persona = TestSubjectProfile(name=persona['name'], color=persona['color'], project_id=instance.id,
                                         order=idx)
            persona.save()
        
        return super(ProjectViewSet, self).perform_create(serializer)

    def perform_update(self, serializer):
        instance = self.get_object()
        posted_data = self.request.data
        if 'use_single_profile' in posted_data and (posted_data['use_single_profile'] == "1" or posted_data['use_single_profile'] == True) and instance.test_subject_profiles.count()==0:
            # create default profile
            for idx, persona in enumerate(settings.DEFAULT_PROFILE):
                persona = TestSubjectProfile(name=persona['name'], color=persona['color'], project_id=instance.id, order=idx)
                persona.save()

        return super(ProjectViewSet, self).perform_update(serializer)

    @detail_route(methods=['get'], permission_classes=[AllowAny])
    def campaigns(self, request, *args, **kwargs):
        logger.debug('In ProjectViewSet campaigns')
        campaigns = Campaign.objects.filter(project__uuid=kwargs['uuid']).all()

        from project.recruit.serializers import CampaignSerializer

        serializer = CampaignSerializer(campaigns, many=True)

        return Response(serializer.data)

    @detail_route(methods=['get'], permission_classes=[AllowAny])
    def personas(self, request, *args, **kwargs):
        logger.debug('In ProjectViewSet personas')
        personas = TestSubjectProfile.objects.filter(project__uuid=kwargs['uuid']).all()

        from project.recruit.serializers import TestSubjectProfileSerializer

        serializer = TestSubjectProfileSerializer(personas, many=True)

        return Response(serializer.data)

    @detail_route(methods=['get'], permission_classes=[AllowAny])
    def public(self, request, *args, **kwargs):
        logger.debug('In ProjectViewSet public')
        try:
            # get hash key from url (uuid is used as param). This is base64 encrypted value.
            hash_key_decoded = base64.b64decode(kwargs['uuid'])
            hash_key_decoded_list = hash_key_decoded.split("|")
            projects = Project.objects.filter(id=hash_key_decoded_list[0]).first()
            # match url hash with salt for more security
            salt = str(projects.uuid)
            if hash_key_decoded_list[1] != salt[:8]:
                raise Http404

            serializer = ProjectSerializer(projects)

            logger.debug('Returning %s' % serializer.data)
            return Response(serializer.data)
        except:
            raise Http404
            
    @detail_route(methods=['get'], permission_classes=[AllowAny])
    def agency_campaigns(self, request, *args, **kwargs):
        logger.debug('In ProjectViewSet agency_campaigns')
        # get hash key from url (uuid is used as param). This is base64 encrypted value.
        try:
            hash_key = kwargs['uuid']
            hash_key_decoded = base64.b64decode(hash_key)
            hash_key_decoded_list = hash_key_decoded.split("|")
            projects = Project.objects.filter(id=hash_key_decoded_list[0]).first()
            # match url hash with salt for more security
            salt = str(projects.uuid)
            if hash_key_decoded_list[2] != salt[:8]:
                raise Http404
                
            campaigns = Campaign.objects.filter(project__id=hash_key_decoded_list[0]).filter(agency__id=hash_key_decoded_list[1]).all()

            from project.recruit.serializers import CampaignSerializer

            serializer = CampaignSerializer(campaigns, many=True)

            return Response(serializer.data)
        except:
            raise Http404
            

class ProjectsList(APIView):
    def get(self, request):
        logger.debug('In ProjectsList get')
        if not request.user.is_authenticated():
            return Response('403 Forbidden', status=status.HTTP_403_FORBIDDEN)

        projects = None
        output = []
        order_by_param = self.request.query_params.get('ordering', None)
        if order_by_param is not None:
            order_by_string = [order_by_param]
        else:
            order_by_string = ['interface__client__account__name', 'interface__client__name', 'interface__name', 'name']

        if request.user.is_staff:
            logger.debug('User is staff')
            current_account = self.request.query_params.get('account', None)
            if current_account is not None:
                projects = Project.objects.filter(interface__client__account_id=current_account).order_by(*order_by_string)
            else:    
                projects = Project.objects.order_by(*order_by_string)

        if request.user.is_authenticated() and request.user.agencies.all():
            logger.debug('request.user.agencies.all=%s' % request.user.agencies.all())
            agency = request.user.agencies.all()[0]
            projects = Project.objects.filter(campaigns__in=agency.campaigns.all()) \
                .order_by(*order_by_string)
        if request.user.is_authenticated() and request.user.clients.all():
            logger.debug('request.user.clients.all=%s' % request.user.clients.all())
            projects = Project.objects.filter(interface__client__in=request.user.clients.all()) \
                .order_by(*order_by_string)
        if request.user.is_authenticated() and request.user.accounts.all():
            logger.debug('request.user.accounts.all=%s' % request.user.accounts.all())
            projects = Project.objects.filter(interface__client__account__in=request.user.accounts.all()) \
                .order_by(*order_by_string)

        # Add pagination to list and overwrite projects object. If no page param is provided then use default projects object without pagination to show all projects
        paginator = Paginator(projects, settings.PROJECTS_PER_PAGE)
        current_page = self.request.query_params.get('page', None)
        if current_page is not None:        
            projects = paginator.page(current_page)
            pagination = { 'total':paginator.count, 'num_pages':paginator.num_pages}    
        else:
            pagination = { 'total':paginator.count, 'num_pages':1}    

        serializer = ProjectListSerializer(projects, many=True)   
        return Response({'projects':serializer.data, 'pagination':pagination}, status=status.HTTP_200_OK)

class UserListView(CreateView):
    form_class = CreateUserForm
    success_url = '/users'
    template_name = 'account/user_list.html'

    def get_context_data(self, **kwargs):
        if self.request.user.is_staff:
            kwargs['object_list'] = User.objects.all()
            kwargs['license'] = None
            kwargs['users'] = None

        if self.request.user.accounts.first():
            kwargs['object_list'] = User.objects.filter(accounts__in=self.request.user.accounts.all()).order_by('id')
            account = self.request.user.accounts.first()
            kwargs['license'] = account.license
            kwargs['users'] = len(kwargs['object_list'])

        context = super(UserListView, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)

        if User.objects.filter(email=obj.email).all():
            form.add_error('email', 'User already exist')
            return self.form_invalid(form)
        else:
            obj.username = obj.email

            obj.save()
            obj.accounts = self.request.user.accounts.all()
            obj.save()

            # send invite email
            # borrow from password_reset packages
            context = {
                'site': get_current_site(self.request),
                'sender': self.request.user.display_name,
                'token': signing.dumps(obj.pk, salt='password_recovery'),
                'secure': self.request.is_secure(),
            }
            body = loader.render_to_string('password_reset/invite_email.txt', context).strip()
            subject = loader.render_to_string('password_reset/invite_email_subject.txt', context).strip()
            send_mail(subject, body, settings.DEFAULT_FROM_EMAIL, [obj.email])

            messages.success(self.request, 'Invitation email has been sent to %s.' % obj.email)

            return HttpResponseRedirect('/users')


class UserDeleteView(DeleteView):
    template_name = 'account/confirm_user_delete.html'
    model = User
    success_url = '/users'

    def get_object(self, queryset=None):
        obj = super(UserDeleteView, self).get_object()

        """ Hook to ensure object is can be deleted by request.user. """
        if not self.request.user.is_staff and obj.accounts.first() not in self.request.user.accounts.all():
            # messages.error(self.request, 'You don\'t have permission to delete user %s.' % obj.display_name)
            raise Http404

        return obj

    def post(self, request, *args, **kwargs):
        self.items_to_delete = self.request.POST.getlist('itemsToDelete')
        if self.request.POST.get("confirm_delete"):
            # when confirmation page has been displayed and confirm button pressed
            user = self.get_object()
            user.delete()  # deleting on the queryset is more efficient than on the model object
            return HttpResponseRedirect(self.success_url)
        elif self.request.POST.get("cancel"):
            # when confirmation page has been displayed and cancel button pressed
            return HttpResponseRedirect(self.success_url)
        else:
            # when data is coming from the form which lists all items
            return self.get(self, *args, **kwargs)


class UserByEmailDetail(APIView):
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = [
        permissions.IsAuthenticated
    ]

    def post(self, request, format=None):

        if 'email' not in request.data:
            return Response(['Missing parameters'], status=status.HTTP_400_BAD_REQUEST)

        email = request.data['email']

        user = User.objects.filter(email=email).first()
        if user:
            serialized = UserSerializer(user)
            return Response(serialized.data, status=status.HTTP_200_OK)
        else:
            return Response(None, status=status.HTTP_200_OK)

# register a account user from web for guest visitor on create API is allow for guest user
class AccountRegistration(CreateAPIView):
    serializer_class = RegisterAccountSerializer
    def create(self, request):
        # take off the signup from public website and return 404 error
        return Response({'message':"Invalid request"}, status=status.HTTP_404_NOT_FOUND)
        serializer = RegisterAccountSerializer(data=request.data)
        if serializer.is_valid():
            # Begin reCAPTCHA validation
            recaptcha_response = request.data['recaptcha_response']
            url = 'https://www.google.com/recaptcha/api/siteverify'
            values = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
            }
            data = urllib.urlencode(values)
            req = urllib2.Request(url, data)
            response = urllib2.urlopen(req)
            result = json.load(response)
            # End reCAPTCHA validation
            if result['success']:
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response({'recaptcha':"Inavlid captcha value"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)       


# class to get or create client and interface
class ClientInterfaceGetOrCreate(CreateAPIView):
    serializer_class = InterfaceSerializer
    def get_permissions(self):
        return (permissions.IsAuthenticated(),)
    
    def create(self, request):
        post_data = request.data
        user = self.request.user
        if user.is_staff:
            account = Account.objects.get(id=post_data['account'])
        else:
            account = Account.objects.get(users=user)

        # account = Account.objects.get(id=post_data['account'])
        if post_data['client'] is None or not post_data['client']:
            client_name = settings.DEFAULT_CLIENT_NAME
        else:
            client_name = post_data['client']   

        if post_data['interface'] is None or not post_data['interface']:
            interface_name = settings.DEFAULT_INTERFACE_NAME
        else:
            interface_name = post_data['interface']
                 

        client, created = Client.objects.get_or_create(name=client_name, account=account)
        interface, created = Interface.objects.get_or_create(name=interface_name, client=client)
        serializer = InterfaceSerializer(interface)
        return Response(serializer.data, status=status.HTTP_201_CREATED)       

# get a project list with scripts
class ProjectsScriptList(APIView):
    def get(self, request):
        logger.debug('In ProjectsScriptList get')
        if not request.user.is_authenticated():
            return Response('403 Forbidden', status=status.HTTP_403_FORBIDDEN)

        user = request.user
        projects = None
        
        # Filter the project with scripts, if count_only parms is passesd in url. i.e. products-scripts/?count_only=1
        script_count_only = self.request.query_params.get('count_only', None)
        queryset = Project.objects.all()
        queryset = queryset.filter(scripts__id__isnull = False).distinct()

        if user.is_staff:
            logger.debug('User is staff')
            projects = queryset
        elif user.is_authenticated() and user.accounts.all():
            logger.debug('request.user.accounts.all=%s' % user.accounts.all())
            projects = queryset.filter(interface__client__account__in=user.accounts.all())
        elif user.is_authenticated() and user.agencies.all():
            logger.debug('request.user.agencies.all=%s' % user.agencies.all())
            agency = user.agencies.all()[0]
            projects = queryset.filter(campaigns__in=agency.campaigns.all())
        elif user.is_authenticated() and user.clients.all():
            logger.debug('request.user.clients.all=%s' % user.clients.all())
            projects = queryset.filter(interface__client__in=user.clients.all())
        else:
            return Response('403 Forbidden', status=status.HTTP_403_FORBIDDEN)

        if script_count_only is not None:
            projects_count = projects.count()
            return Response({'projects_count':projects_count}, status=status.HTTP_200_OK)        
           
        # Add pagination to list and overwrite projects object. If no page param is provided then use default projects object without pagination to show all projects
        paginator = Paginator(projects, settings.PROJECTS_PER_PAGE)
        current_page = self.request.query_params.get('page', None)
        if current_page is not None:        
            projects = paginator.page(current_page)
            pagination = { 'total':paginator.count, 'num_pages':paginator.num_pages}    
        else:
            pagination = { 'total':paginator.count, 'num_pages':1}    

        serializer = ProjectSerializer(projects, many=True)   
        return Response({'projects':serializer.data, 'pagination':pagination}, status=status.HTTP_200_OK)        


class ExampleProject(APIView):   
    permission_classes = [permissions.IsAuthenticated]
    def get_object(self, uuid):
        try:
            return Project.objects.get(uuid=uuid)
        except Project.DoesNotExist:
            raise Http404

    def get(self, request, format=None):
        user = self.request.user
        uuid = settings.DEFAULT_DEMO_PROJECT
        account = Account.objects.filter(users=user).first()
        project_default = self.get_object(uuid)
        output = []
        zoneList = project_default.zones.all()
        eventList = project_default.events.all()
        navigationList = project_default.navigation.all()
        test_subject_profile_list = project_default.test_subject_profiles.all()
        campaign_list = project_default.campaigns.all()
        script_list = project_default.scripts.all()
        script_ids = []
        for script in script_list:
            script_d = Script.objects.get(pk=script.id)
            script_ids.append({'id':script_d.id})

        client, created = Client.objects.get_or_create(name=project_default.interface.client.name, account=account)
        client_id = client.id
       
        interface, created = Interface.objects.get_or_create(name=project_default.interface.name, client=client)
        interface_id = interface.id
        
        # create default project
        project = Project(name=project_default.name)
        project_uuid = project.uuid
        project = project_default
        project.pk = None
        project.uuid = project_uuid
        project.interface = interface
        project.save()

        # create default zone
        for zone_detail in zoneList:
            zone = Zone(project_id=project.id, name = zone_detail.name, order = zone_detail.order)
            zone.save()
            # output.append({'name':zones.name, 'id':zones.id})

        # create default event
        for event_detail in eventList:
            event = Event(name=event_detail.name, project_id=project.id, order=event_detail.order)
            event.save()

        # create default profile
        for persona in test_subject_profile_list:
            persona = TestSubjectProfile(name=persona.name, color=persona.color, project_id=project.id, order=persona.order)
            persona.save()

        # create default campaigns
        for campaign_detail in campaign_list:
            # create agency for compaign
            agency = Agency()
            agency = campaign_detail.agency
            agency.pk = None
            agency.account = account
            agency.save()
            compaign = Campaign()
            compaign.pk = None
            compaign.location = campaign_detail.location
            compaign.start = campaign_detail.start
            compaign.end = campaign_detail.end
            compaign.duration = campaign_detail.duration
            compaign.tester_required = campaign_detail.tester_required
            compaign.project = project
            compaign.agency = agency
            compaign.save()   
            # check and create compaign sessions
            for session_detail in campaign_detail.sessions.order_by('datetime'):
                session = Session()
                session.pk = None
                if session_detail.test_subject_first_name=='Lunch':
                    session.test_subject_first_name = session_detail.test_subject_first_name
                session.campaign = compaign
                session.datetime = session_detail.datetime
                session.save()  

        # return Response(output, status=status.HTTP_200_OK)  
        serializer = ProjectSerializer(project)
        return Response({'project':serializer.data, 'scripts':script_ids})        


class AccountTypesList(APIView):
    def get(self, request):
        account_types = []
        for idx, account_type in enumerate(ACCOUNT_TYPES):
            account_types.append({'value':account_type[0], 'display_name':account_type[1], 'no_of_license':ACCOUNT_TYPES_LICENSES[idx][1]})    
        
        return Response(account_types, status=status.HTTP_200_OK)


class AccountDetail(APIView):
    def get(self, request):
        if not request.user.is_authenticated():
            return Response('403 Forbidden', status=status.HTTP_403_FORBIDDEN)

        """ Get the account details for requested user. """
        if self.request.user.accounts.first():
            account = self.request.user.accounts.first()
            data = {'license':account.license}
            return Response(data, status=status.HTTP_200_OK)   

        return Response(None, status=status.HTTP_200_OK)    