from django.conf.urls import url

from .views import ProjectView, UserListView, UserDeleteView, UserByEmailDetail, AccountRegistration

urlpatterns = [
    url(r'^project/add', ProjectView.as_view(), name='project-add'),
    url(r'^project/<uuid>/edit', ProjectView.as_view(), name='project-edit'),
    # url(r'^users$', UserListView.as_view(), name='users-list'),
    url(r'^user/(?P<pk>\d+)/delete', UserDeleteView.as_view(), name='user-delete'),
    url(r'^user/email$', UserByEmailDetail.as_view(), name='user-email'),
    url(r'^accounts/registration$', AccountRegistration.as_view(), name='accounts-registration'),
]
