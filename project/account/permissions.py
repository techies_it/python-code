import logging

from rest_framework import permissions

logger = logging.getLogger(__name__)


class IsAccountOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user:
            if request.user.is_staff:
                logger.debug('staff user, returning true in IsAccountOwner')
                return True
            logger.debug('obj.owner=%s' % obj.owner)
            logger.debug('request.user=%s' % request.user)
            obj_owner_is_user = obj.owner == request.user
            logger.debug('Returning %s IsAccountOwner' % obj_owner_is_user)
            return obj_owner_is_user
        logger.debug('No user in request, returning false IsAccountOwner')
        return False


class IsUserAccessible(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user:
            # super admin
            if request.user.is_staff:
                logger.debug('Staff user, returning true IsUserAccessible')
                return True
            # is user
            if obj == request.user:
                logger.debug('User object, returning true IsUserAccessible')
                return True
            # same account
            logger.debug('obj.accounts.all=%s' % obj.accounts.all())
            logger.debug('request.user.accounts.all=%s' % request.user.accounts.all())
            for account in obj.accounts.all():
                if account in request.user.accounts.all():
                    logger.debug('Returning true IsUserAccessible')
                    return True
            # account agency user
            logger.debug('obj.agencies.all=%s' % obj.agencies.all())
            for agency in obj.agencies.all():
                if agency.account in request.user.accounts.all():
                    logger.debug('Returning true IsUserAccessible')
                    return True
            # account client user
            logger.debug('obj.clients.all=%s' % obj.clients.all())
            for client in obj.clients.all():
                if client.account in request.user.accounts.all():
                    logger.debug('Returning true IsUserAccessible')
                    return True

        logger.debug('Returning false IsUserAccessible')
        return False


class IsProjectAccessible(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user:
            # super admin
            if request.user.is_staff:
                logger.debug('Staff user, returning true IsProjectAccessible')
                return True
            # same account
            logger.debug('obj.interface.client.account=%s' % obj.interface.client.account)
            logger.debug('request.user.accounts.all=%s' % request.user.accounts.all())
            if obj.interface.client.account in request.user.accounts.all():
                logger.debug('Returning true IsProjectAccessible')
                return True
            # recruiter
            logger.debug('obj.campaigns.all=%s' % obj.campaigns.all())
            logger.debug('request.user.agencies.all=%s' % request.user.agencies.all())
            for campaign in obj.campaigns.all():
                if request.user.agencies.all() and request.user.agencies.all()[0] == campaign.agency:
                    logger.debug('Returning true IsProjectAccessible')
                    return True
            # client
            logger.debug('obj.interface.client=%s' % obj.interface.client)
            logger.debug('request.user.clients.all=%s' % request.user.clients.all())
            if obj.interface.client in request.user.clients.all():
                logger.debug('Returning true IsProjectAccessible')
                return True

        logger.debug('Returning false IsProjectAccessible')
        return False
