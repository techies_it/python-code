from django.forms import ModelForm

from .models import User


class CreateUserForm(ModelForm):
    class Meta:
        model = User
        fields = ['email']

    def __init__(self, *args, **kwargs):
        super(CreateUserForm, self).__init__(*args, **kwargs)

        self.fields['email'].required = True

    def clean_username(self):
        data = self.cleaned_data['email']
        return data
