from django.conf import settings
from django.conf.urls import include, url
# Uncomment the next two lines to enable the admin:
from django.contrib import admin

from .views import IndexView

admin.autodiscover()

from password_reset import urls as password_reset_urls

urlpatterns = [
    url(r'^api/', include('project.api.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^password/', include(password_reset_urls)),
    url(r'^', include('project.account.urls')),
    url(r'^', include('project.capacitor.urls')),
    url(r'^', include('project.recruit.urls')),
    url(r'^', include('project.report.urls')),
    url(r'^hijack/', include('hijack.urls')),

    # pass below to angular routing
    # url('^.*$', IndexView.as_view(), name='index'),
    url('^$', IndexView.as_view(), name='index'),
    url('^register$', IndexView.as_view(), name='index'),
    url('^login$', IndexView.as_view(), name='index'),
    url('^logout$', IndexView.as_view(), name='index'),
    url('^profile', IndexView.as_view(), name='index'),
    url('^observer', IndexView.as_view(), name='index'),
    url('^accounts$', IndexView.as_view(), name='index'),
    url('^accounts/add$', IndexView.as_view(), name='index'),
    url('^accounts/register$', IndexView.as_view(), name='index'),
    url('^accounts/(?P<uuid>.*)/edit$', IndexView.as_view(), name='index'),
    url('^projects$', IndexView.as_view(), name='index'),
    url('^project/(?P<uuid>.*)/edit$', IndexView.as_view(), name='index'),
    url('^project/(?P<uuid>.*)/recruiters$', IndexView.as_view(), name='index'),
    url('^project/(?P<uuid>.*)/zones$', IndexView.as_view(), name='index'),
    url('^project/(?P<uuid>.*)/events$', IndexView.as_view(), name='index'),
    url('^project/(?P<uuid>.*)/navigation$', IndexView.as_view(), name='index'),
    url('^project/(?P<uuid>.*)/test-subject-profiles$', IndexView.as_view(), name='index'),
    url('^project/(?P<uuid>.*)/tasks$', IndexView.as_view(), name='index'),
    url('^project/(?P<uuid>.*)/scripts$', IndexView.as_view(), name='index'),
    url('^project/(?P<uuid>.*)/script/add$', IndexView.as_view(), name='index'),
    url('^project/(?P<uuid>.*)/script/(?P<sid>\d+)/edit$', IndexView.as_view(), name='index'),
    url('^project/(?P<uuid>.*)/report$', IndexView.as_view(), name='index'),
    url('^project/(?P<uuid>.*)/progress$', IndexView.as_view(), name='index'),
    url('^recruiters$', IndexView.as_view(), name='index'),
    url('^recruiter/add$', IndexView.as_view(), name='index'),
    url('^recruiter/(?P<id>.*)/edit$', IndexView.as_view(), name='index'),
    url('^client/add$', IndexView.as_view(), name='index'),
    url('^client/(?P<id>.*)/edit$', IndexView.as_view(), name='index'),
    url('^reports$', IndexView.as_view(), name='index'),
    url('^public-report$', IndexView.as_view(), name='index'),
    url('^report$', IndexView.as_view(), name='index'),
    url('^recruit$', IndexView.as_view(), name='index'),
    url('^project/(?P<uuid>.*)/session/(?P<sid>.*)script/(?P<id>.*)$', IndexView.as_view(), name='index'),
    url('^terms-and-conditions$', IndexView.as_view(), name='index'),
    url('privacy-policy', IndexView.as_view(), name='index'),
    url('features', IndexView.as_view(), name='index'),
    url('support', IndexView.as_view(), name='index'),
    url('^users$', IndexView.as_view(), name='index'),

]

if settings.DEBUG:
    from django.views.static import serve

    urlpatterns += [
        url(r'^(?P<path>favicon\..*)$', serve, {'document_root': settings.STATIC_ROOT}),
        url(r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], serve, {'document_root': settings.MEDIA_ROOT}),
        url(r'^%s(?P<path>.*)$' % settings.STATIC_URL[1:], serve, dict(insecure=True)),
    ]
