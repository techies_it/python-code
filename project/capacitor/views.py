import datetime
import logging
import os

import pdfkit
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.template.loader import get_template
from django.views.generic import View
from rest_framework import generics, permissions, authentication
from rest_framework import status
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView

from project.account.models import Project
from project.capacitor.models import ScriptProfile
from project.recruit.models import Session, TestSubjectProfile
from project.report.models import QuestionResult, QuestionOptionResult
from .models import Script, Step, StepItem, Instruction, Note, Question, \
    QuestionOption, Event, Zone, Navigation, Task
from .permissions import IsSameAccount
from .serializers import StepSerializer, EventSerializer, ZoneSerializer, NavigationSerializer, TaskSerializer

logger = logging.getLogger(__name__)


class ScriptDownloadView(View):
    def get(self, request, id, sid):
        if sid:
            uuid = id
            project = get_object_or_404(Project, uuid=uuid)

        script = Script.objects.filter(id=sid).first()

        template = get_template("capacitor/script-pdf.html")
        context = {
            'project': project,
            'script': script,
            'base_url': request.build_absolute_uri("/").strip("/")}
        html = template.render(context)

        #        return HttpResponse(html,content_type='text/html')

        now = datetime.datetime.now()

        options = {
            'page-size': 'A4',
            'margin-top': '0.75in',
            'margin-right': '0.75in',
            'margin-bottom': '0.75in',
            'margin-left': '0.75in',
            'encoding': "UTF-8",
            #            'header-spacing': 5,
            #            'header-line': '',
            #            'header-font-size': 12,
            #            'header-right': 'Usability Test Results',
            #            'footer-spacing': 5,
            #            'footer-line': '',
            #            'footer-font-size': 10,
            'footer-right': '[page]/[topage]',
            'footer-center': 'Copyright %s Flex UX' % now.year,
            'dpi': 300,
            'quiet': ''
        }

        toc = {
            'toc-header-text': 'Contents'
        }

        pdf = pdfkit.from_string(html, False, options=options)

        filename = 'Script - ' + project.name + ' - ' + script.name + ' - '
        filename += now.strftime('%d%m%Y')

        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="' + filename + '.pdf"'

        return response


class ScriptAjaxAddView(APIView):
    def post(self, request, format=None):

        if 'project' not in request.data or 'script' not in request.data:
            return Response(['Missing parameters'], status=status.HTTP_400_BAD_REQUEST)

        project_uuid = request.data['project']
        script = request.data['script']

        project = Project.objects.filter(uuid=project_uuid).first()

        # check access permission
        if not request.user.is_staff and project.interface.client.account not in request.user.accounts.all():
            return Response(['You don\'t have permission to perform this action'], status=status.HTTP_400_BAD_REQUEST)

        # validation
        validation_error = []
        if not script['name']:
            validation_error.append('Script name is required')
        for idx, step in enumerate(script['steps']):
            if not step['name']:
                validation_error.append('Step ' + str(idx + 1) + ' name is required')
            for idx2, item in enumerate(step['items']):
                if item['type'] == 'instruction' or item['type'] == 'note' or item['type'] == 'task':
                    if not item['content']:
                        validation_error.append('Step' + str(idx + 1) + ' ' + item['type'] + ' content is required')
                else:
                    if not item['name']:
                        validation_error.append('Step' + str(idx + 1) + ' ' + 'question name is required')
                    for option in item['options']:
                        if not option['name']:
                            validation_error.append('Step' + str(idx + 1) + ' ' + 'question choice value is required')
        if validation_error:
            return Response(validation_error, status=status.HTTP_400_BAD_REQUEST)

        try:

            # create script obj
            new_script = Script(
                project=project,
                name=script['name'],
                scenario=script['scenario'])
            new_script.save()

            # update default profiles
            for profile_id in script['default_profiles']:
                profile = TestSubjectProfile.objects.get(pk=profile_id)
                script_profile = ScriptProfile(
                    script=new_script,
                    profile=profile
                )
                script_profile.save()

            # handle removed default profile
            for sp in ScriptProfile.objects.filter(script=new_script).all():
                if sp.profile.id not in script['default_profiles']:
                    sp.delete()

            for idx, step in enumerate(script['steps']):

                default_zone = None
                if 'default_zone' in step and step['default_zone'] != '':
                    default_zone = Zone.objects.get(pk=step['default_zone'])

                # create step obj
                new_step = Step(
                    script=new_script,
                    name=step['name'],
                    default_zone=default_zone,
                    order=idx)
                new_step.save()

                for idx2, item in enumerate(step['items']):

                    new_item = StepItem(step=new_step, order=idx2)
                    new_item.save()

                    if item['type'] == 'instruction':
                        new_instr = Instruction(
                            step_item=new_item,
                            content=item['content'])
                        new_instr.save()

                    if item['type'] == 'note':
                        new_note = Note(
                            step_item=new_item,
                            content=item['content'])
                        new_note.save()

                    if item['type'] == 'task':
                        new_task = Task(
                            step_item=new_item,
                            content=item['content'])
                        new_task.save()

                    if item['type'] == 'question':
                        new_quest = Question(
                            step_item=new_item,
                            name=item['name'],
                            is_multi=item['is_multi'],
                            option_type=item['option_type'],
                            note=item['note'])
                        new_quest.save()
                        if item['option_type'] == 'sus':
                            for idx4, sus_question in enumerate(settings.SUS_QUESTIONS):
                                new_sus_item = StepItem(step=new_step, order=idx2, child_order=idx4+1)
                                new_sus_item.save()
                                new_sus_quest = Question(
                                    step_item=new_sus_item,
                                    name=sus_question,
                                    option_type='sus',
                                    parent=new_quest)
                                new_sus_quest.save()
                                for idx5, sus_question_option in enumerate(settings.SUS_QUESTION_OPTIONS):
                                    new_option = QuestionOption(
                                        question=new_sus_quest,
                                        name=sus_question_option['name'],
                                        value=sus_question_option['value'],
                                        order=idx5)
                                    new_option.save()
                        else:    
                            for idx3, option in enumerate(item['options']):
                                new_option = QuestionOption(
                                    question=new_quest,
                                    name=option['name'],
                                    order=idx3)
                                new_option.save()

        except Exception as e:
            new_script.delete()
            return Response([e.message], status=status.HTTP_400_BAD_REQUEST)

        return Response({'id':new_script.id}, status=status.HTTP_201_CREATED)


class ScriptAjaxLoadView(APIView):
    def get(self, request, id):
        script = Script.objects.get(pk=id)
        session_id = request.GET.get('sid')
        edit_page = request.GET.get('edit')
        filtered_profile_name = request.GET.get('profile')

        # check script exists
        if not script:
            return Response('404 Not Found', status=status.HTTP_404_NOT_FOUND)

        #### ALLOW anonymous for public report ####
        # check user have access to script and allow demo script to all logged user
        if request.user.is_authenticated():
            if script.project.id != settings.DEFAULT_DEMO_PROJECT_ID:
                if not request.user.is_staff \
                        and script.project.interface.client.account not in request.user.accounts.all() \
                        and script.project.interface.client not in request.user.clients.all():
                    return Response('403 Forbidden', status=status.HTTP_403_FORBIDDEN)

        output = {
            'id': script.id,
            'name': script.name,
            'scenario': script.scenario,
            'default_profiles': [],
            'steps': []}

        for profile in script.default_profiles.all():
            output['default_profiles'].append(profile.id)

        for step in script.steps.order_by('order'):
            o_step = {
                'id': step.id,
                'name': step.name,
                'default_zone': step.default_zone and step.default_zone.id or '',
                'items': []}

            for item in step.items.order_by('order','child_order'):
                if hasattr(item, 'instruction'):
                    o_item = {
                        'id': item.id,
                        'type': 'instruction',
                        'content': item.instruction.content}

                if hasattr(item, 'note'):
                    o_item = {
                        'id': item.id,
                        'type': 'note',
                        'content': item.note.content}

                if hasattr(item, 'task'):
                    o_item = {
                        'id': item.id,
                        'type': 'task',
                        'content': item.task.content}

                    o_item['elapsed'] = ''
                    o_item['status'] = ''
                    if session_id:
                        if session_id == 'all':
                            if filtered_profile_name:
                                results = item.task.getResultByAllSessionFiltered(filtered_profile_name)
                            else:    
                                results = item.task.getResultByAllSession

                            o_item['result'] = []
                            for result in results:
                                if result and result.session.display_name and result.elapsed_time_formatted and result.status:
                                    temp = {}
                                    temp['session'] = result and result.session.display_name or ''
                                    temp['session_profile'] = result and result.session.test_subject_profile.name or ''
                                    temp[
                                        'session_color'] = result and result.session.test_subject_profile.color.lower() or ''
                                    temp['used'] = result and result.used_time_formatted or ''
                                    temp['elapsed'] = result and result.elapsed_time_formatted or ''
                                    temp['status'] = result and result.status or ''
                                    temp['session_id'] = result and result.session_id or ''
                                    temp['session_executed_by_name'] = result and result.session.executed_by_name or ''
                                    o_item['result'].append(temp)
                        else:
                            result = item.task.getResultBySession(session_id)
                            o_item['used'] = result and result.used_time_formatted or ''
                            o_item['status'] = result and result.status or ''
                            o_item['taskresult_id'] = result and result.id or ''

                if hasattr(item, 'question'):
                    if edit_page:
                        if not item.question.is_parent:
                            continue
                    o_item = {
                        'id': item.id,
                        'type': 'question',
                        'name': item.question.name,
                        'is_multi': item.question.is_multi,
                        'option_type': item.question.option_type,
                        'note': item.question.note,
                        'is_parent': item.question.is_parent,
                        'parent_id': item.question.parent_id,
                        'options': []}

                    o_item['response_id'] = ''
                    o_item['response'] = ''
                    if session_id:
                        if session_id == 'all':
                            if filtered_profile_name:
                                results = item.question.getResultByAllSessionFiltered(filtered_profile_name)
                            else:
                                results = item.question.getResultByAllSession
                            o_item['result'] = []
                            if item.question.option_type == 'nps' or (item.question.option_type == 'sus' and item.question.is_parent):
                                o_item['nps_sus_score'] = item.question.getNpsSusScore(filtered_profile_name)
                            for result in results:
                                if result and result.session.display_name and result.response:
                                    temp = {}
                                    temp['session'] = result and result.session.display_name or ''
                                    temp['session_profile'] = result and result.session.test_subject_profile.name or ''
                                    temp['session_color'] = result and result.session.test_subject_profile.color.lower() or ''
                                    temp['elapsed'] = result and result.elapsed_time_formatted or ''
                                    temp['response'] = result and result.response or ''
                                    temp['session_id'] = result and result.session_id or ''
                                    temp['session_executed_by_name'] = result and result.session.executed_by_name or ''
                                    o_item['result'].append(temp)
                        else:
                            result = item.question.getResultBySession(session_id)
                            o_item['response_id'] = result and result.id or ''
                            o_item['response'] = result and result.response or ''

                    options = item.question.question_options.order_by('order')

                    ## update navigation if it is changed:
                    if item.question.option_type == 'navigation':
                        new_options = [];
                        for nav in script.project.navigation.order_by('order'):
                            if nav.name not in [o.name for o in options]:
                                new_option = QuestionOption(
                                    question=item.question,
                                    name=nav.name,
                                    order=nav.order)
                                new_option.save()
                                new_options.append(new_option)
                            else:
                                for o in options:
                                    if o.name == nav.name:
                                        o.order = nav.order
                                        o.save()
                                        new_options.append(o)

                        options = new_options

                    for option in options:
                        o_option = {
                            'id': option.id,
                            'name': option.name}
                        if item.question.option_type == 'sus':
                            o_option['value'] = option.value

                        o_option['response_id'] = ''
                        o_option['response'] = ''
                        if session_id:
                            if session_id == 'all':
                                if filtered_profile_name:
                                    results = option.getResultByAllSessionFiltered(filtered_profile_name)
                                else:
                                    results = option.getResultByAllSession
                                o_option['result'] = []
                                for result in results:
                                    if result and result.session.display_name and result.response:
                                        temp = {}
                                        temp['session'] = result and result.session.display_name or ''
                                        temp[
                                            'session_profile'] = result and result.session.test_subject_profile.name or ''
                                        temp[
                                            'session_color'] = result and result.session.test_subject_profile.color.lower() or ''
                                        temp['elapsed'] = result and result.elapsed_time_formatted or ''
                                        temp['response'] = result and result.response or ''
                                        temp['session_executed_by_name'] = result and result.session.executed_by_name or ''
                                        o_option['result'].append(temp)
                            else:
                                result = option.getResultBySession(session_id)
                                o_option['response_id'] = result and result.id or ''
                                o_option['response'] = result and result.response or ''

                        o_item['options'].append(o_option)
                o_step['items'].append(o_item)
            output['steps'].append(o_step)

        return Response(output, status=status.HTTP_200_OK)

    def delete(self, request, id, format=None):
        script = Script.objects.get(pk=id)

        # check script exists
        if not script:
            return Response('404 Not Found', status=status.HTTP_404_NOT_FOUND)

        # check user have access to script
        if not request.user.is_staff and script.project.interface.client.account not in request.user.accounts.all():
            return Response('403 Forbidden', status=status.HTTP_403_FORBIDDEN)

        script.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ScriptAjaxUpdateView(APIView):
    def post(self, request, id):
        script = Script.objects.get(pk=id)

        # check script exists
        if not script:
            return Response('404 Not Found', status=status.HTTP_404_NOT_FOUND)

        # check access permission
        if not request.user.is_staff and script.project.interface.client.account not in request.user.accounts.all():
            return Response(['You don\'t have permission to perform this action'], status=status.HTTP_400_BAD_REQUEST)

        script_data = request.data['script']
        project_uuid = request.data['uuid']

        project = Project.objects.filter(uuid=project_uuid).first()

        # validation
        validation_error = []
        if not script_data['name']:
            validation_error.append('Script name is required')
        for idx, step in enumerate(script_data['steps']):
            if not step['name']:
                validation_error.append('Step ' + str(idx + 1) + ' name is required')
            for idx2, item in enumerate(step['items']):
                if item['type'] == 'instruction' or item['type'] == 'note' or item['type'] == 'task':
                    if not item['content']:
                        validation_error.append('Step' + str(idx + 1) + ' ' + item['type'] + ' content is required')
                else:
                    if not item['name']:
                        validation_error.append('Step' + str(idx + 1) + ' ' + 'question name is required')
                    for option in item['options']:
                        if not option['name']:
                            validation_error.append('Step' + str(idx + 1) + ' ' + 'question choice value is required')
        if validation_error:
            return Response(validation_error, status=status.HTTP_400_BAD_REQUEST)

        try:

            # update script obj
            script.name = script_data['name']
            script.scenario = script_data['scenario']
            script.save()

            # update default profiles
            for profile_id in script_data['default_profiles']:
                profile = TestSubjectProfile.objects.get(pk=profile_id)
                exists = ScriptProfile.objects.filter(script=script, profile=profile).first()
                if not exists:
                    script_profile = ScriptProfile(
                        script=script,
                        profile=profile
                    )
                    script_profile.save()

            # handle removed default profile
            for sp in ScriptProfile.objects.filter(script=script).all():
                if sp.profile.id not in script_data['default_profiles']:
                    sp.delete()

            for idx, step in enumerate(script_data['steps']):

                default_zone = None
                if 'default_zone' in step and step['default_zone'] != '':
                    default_zone = Zone.objects.get(pk=step['default_zone'])

                # load step obj
                if 'id' in step:
                    obj_step = Step.objects.get(pk=step['id'])
                    obj_step.name = step['name']
                    obj_step.default_zone = default_zone
                    obj_step.order = idx
                    obj_step.save()
                else:
                    obj_step = Step.objects.create(
                        script=script,
                        name=step['name'],
                        default_zone=default_zone,
                        order=idx)
                    script_data['steps'][idx]['id'] = obj_step.id

                new_sus_question_items = [] #variable to hold ids of newly created step items for SUS questions 
                for idx2, item in enumerate(step['items']):

                    # load item obj
                    if 'id' in item:
                        obj_item = StepItem.objects.get(pk=item['id'])
                        obj_item.order = idx2
                        obj_item.save()
                    else:
                        obj_item = StepItem.objects.create(
                            step=obj_step,
                            order=idx2)
                        step['items'][idx2]['id'] = obj_item.id

                    if item['type'] == 'instruction':
                        if hasattr(obj_item, 'instruction'):
                            obj_instr = obj_item.instruction
                            obj_instr.content = item['content']
                            obj_instr.save()
                        else:
                            obj_instr = Instruction.objects.create(
                                step_item=obj_item,
                                content=item['content'])

                    if item['type'] == 'note':
                        if hasattr(obj_item, 'note'):
                            obj_note = obj_item.note
                            obj_note.content = item['content']
                            obj_note.save()
                        else:
                            obj_note = Note.objects.create(
                                step_item=obj_item,
                                content=item['content'])

                    if item['type'] == 'task':
                        if hasattr(obj_item, 'task'):
                            obj_task = obj_item.task
                            obj_task.content = item['content']
                            obj_task.save()
                        else:
                            obj_task = Task.objects.create(
                                step_item=obj_item,
                                content=item['content'])

                    if item['type'] == 'question':
                        if hasattr(obj_item, 'question'):
                            obj_quest = obj_item.question
                            obj_quest.name = item['name']
                            obj_quest.is_multi = item['is_multi']
                            obj_quest.option_type = item['option_type']
                            obj_quest.note = item['note']
                            obj_quest.save()
                            if item['option_type'] == 'sus':
                                # if current question is child then set order as parent order value
                                if item['parent_id'] and item['is_parent'] == False:
                                    obj_item.order = obj_quest.parent.step_item.order
                                    obj_item.save()

                                for sus_question in obj_quest.child_questions.order_by('step_item_id'):
                                    # set order for child sus questions
                                    obj_sus_item = StepItem.objects.get(pk=sus_question.step_item_id)
                                    obj_sus_item.order = idx2
                                    obj_sus_item.save()
                                    new_sus_question_items.append(sus_question.step_item_id)
                        else:
                            if item['option_type'] != 'sus' or item['is_parent'] == True:
                                obj_quest = Question.objects.create(
                                    step_item=obj_item,
                                    name=item['name'],
                                    is_multi=item['is_multi'],
                                    option_type=item['option_type'],
                                    note=item['note'])

                                #  handle SUS sub questions on add new SUS at edit script page
                                if obj_quest.step_item and item['option_type'] == 'sus' and item['is_parent'] == True:
                                    for idx4, sus_question in enumerate(settings.SUS_QUESTIONS):
                                        new_sus_item = StepItem.objects.create(step=obj_step,order=idx2, child_order=idx4+1)
                                        new_sus_question_items.append(new_sus_item.id)
                                        new_sus_quest = Question(
                                            step_item=new_sus_item,
                                            name=sus_question,
                                            option_type='sus',
                                            parent=obj_quest)
                                        new_sus_quest.save()
                                        for idx5, sus_question_option in enumerate(settings.SUS_QUESTION_OPTIONS):
                                            new_option = QuestionOption(
                                                question=new_sus_quest,
                                                name=sus_question_option['name'],
                                                value=sus_question_option['value'],
                                                order=idx5)
                                            new_option.save()
                        
                        for idx3, option in enumerate(item['options']):
                            if 'id' in option:
                                obj_option = QuestionOption.objects.get(pk=option['id'])
                                obj_option.name = option['name']
                                obj_option.order = idx3
                                obj_option.save()
                            else:
                                obj_option = QuestionOption.objects.create(
                                    question=obj_quest,
                                    name=option['name'],
                                    order=idx3)
                                item['options'][idx3]['id'] = obj_option.id

                        # handle removed option
                        for option in obj_quest.question_options.all():
                            if option.id not in [o['id'] for o in item['options']]:
                                option.delete()

                # handle removed step item
                for item in obj_step.items.all():
                    if item.id not in [i['id'] for i in step['items']]:
                        if item.id not in new_sus_question_items:
                            item.delete()

            # handle removed step
            for step in script.steps.all():
                if step.id not in [s['id'] for s in script_data['steps']]:
                    step.delete()

        except Exception as e:
            return Response([e.message], status=status.HTTP_400_BAD_REQUEST)

        return Response('success', status=status.HTTP_200_OK)


class EventMixin(object):
    model = Event
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = [
        permissions.IsAuthenticated,
        IsSameAccount,
    ]


class EventList(EventMixin, generics.ListCreateAPIView):
    pass


class EventDetail(EventMixin, generics.RetrieveUpdateDestroyAPIView):
    pass


class ProjectEventList(generics.ListAPIView):
    model = Event
    queryset = Event.objects.all()
    serializer_class = EventSerializer

    def get_queryset(self):
        queryset = super(ProjectEventList, self).get_queryset()
        return queryset.filter(project__pk=self.kwargs.get('pk'))


class ZoneMixin(object):
    model = Zone
    queryset = Zone.objects.all()
    serializer_class = ZoneSerializer
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = [
        permissions.IsAuthenticated,
        IsSameAccount,
    ]


class ZoneList(ZoneMixin, generics.ListCreateAPIView):
    pass


class ZoneDetail(ZoneMixin, generics.RetrieveUpdateDestroyAPIView):
    pass


class ZoneStepList(generics.ListAPIView):
    model = Step
    queryset = Step.objects.all()
    serializer_class = StepSerializer

    def get_queryset(self):
        queryset = super(ZoneStepList, self).get_queryset()
        return queryset.filter(default_zone__pk=self.kwargs.get('pk'))


class ProjectZoneList(generics.ListAPIView):
    model = Zone
    queryset = Zone.objects.all()
    serializer_class = ZoneSerializer

    def get_queryset(self):
        queryset = super(ProjectZoneList, self).get_queryset()
        return queryset.filter(project__pk=self.kwargs.get('pk'))


class NavigationMixin(object):
    model = Navigation
    queryset = Navigation.objects.all()
    serializer_class = NavigationSerializer
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = [
        permissions.IsAuthenticated,
        IsSameAccount,
    ]


class NavigationList(NavigationMixin, generics.ListCreateAPIView):
    pass


class NavigationDetail(NavigationMixin, generics.RetrieveUpdateDestroyAPIView):
    pass


class ProjectNavigationList(generics.ListAPIView):
    model = Navigation
    queryset = Navigation.objects.all()
    serializer_class = NavigationSerializer

    def get_queryset(self):
        queryset = super(ProjectNavigationList, self).get_queryset()
        return queryset.filter(project__pk=self.kwargs.get('pk'))


class TaskMixin(object):
    model = Task
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = [
        permissions.IsAuthenticated
    ]


class TaskList(TaskMixin, generics.ListCreateAPIView):
    pass


class TaskDetail(TaskMixin, generics.RetrieveUpdateDestroyAPIView):
    pass


class ProjectTaskList(generics.ListAPIView):
    model = Task
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def get_queryset(self):
        queryset = super(ProjectTaskList, self).get_queryset()
        return queryset.filter(project__uuid=self.kwargs.get('uuid'))


class QuestionResultAjaxUpdateView(APIView):
    def post(self, request):
        question_id = request.data['qid']
        session_id = request.data['sid']
        response = request.data['response']
        elapsed_time = request.data['elapsed_time']

        # check question exists
        question = Question.objects.get(pk=question_id)
        if not question:
            return Response('404 Not Found', status=status.HTTP_404_NOT_FOUND)

        # check session exists
        session = Session.objects.get(pk=session_id)
        if not session:
            return Response('404 Not Found', status=status.HTTP_404_NOT_FOUND)

        # check access permission
        if not request.user.is_staff and question.step_item.step.script.project.interface.client.account not in request.user.accounts.all():
            return Response(['You don\'t have permission to perform this action'], status=status.HTTP_400_BAD_REQUEST)

        # check QuestionResult exists
        result = QuestionResult.objects.filter(question=question, session=session).first()
        if not result:
            result = QuestionResult()
            result.question = question
            result.session = session

        result.response = response
        result.elapsed_time = elapsed_time

        try:
            result.save()
        except Exception as e:
            return Response([e.message], status=status.HTTP_400_BAD_REQUEST)

        return Response('success', status=status.HTTP_200_OK)


class QuestionOptionResultAjaxUpdateView(APIView):
    def post(self, request):
        option_id = request.data['oid']
        session_id = request.data['sid']
        response = request.data['response']
        elapsed_time = request.data['elapsed_time']

        # check option exists
        option = QuestionOption.objects.get(pk=option_id)
        if not option:
            return Response('404 Not Found', status=status.HTTP_404_NOT_FOUND)

        # check session exists
        session = Session.objects.get(pk=session_id)
        if not session:
            return Response('404 Not Found', status=status.HTTP_404_NOT_FOUND)

        # check access permission
        if not request.user.is_staff and option.question.step_item.step.script.project.interface.client.account not in request.user.accounts.all():
            return Response(['You don\'t have permission to perform this action'], status=status.HTTP_400_BAD_REQUEST)

        # check QuestionResult exists
        result = QuestionOptionResult.objects.filter(option=option, session=session).first()
        if not result:
            result = QuestionOptionResult()
            result.option = option
            result.session = session

        result.response = response
        result.elapsed_time = elapsed_time

        try:
            result.save()
        except Exception as e:
            return Response([e.message], status=status.HTTP_400_BAD_REQUEST)

        return Response('success', status=status.HTTP_200_OK)


class ZoneScreenshotUploadView(APIView):
    parser_classes = (FormParser, MultiPartParser,)

    def put(self, request, format=None):
        # check access
        if not request.user.is_staff and not request.user.is_authenticated:
            return Response(['You don\'t have permission to perform this action'], status=status.HTTP_403_FORBIDDEN)

        file_obj = request.data['file']
        zone_id = request.data['zone']

        zone = get_object_or_404(Zone, pk=zone_id)
        folder = str(zone.project.uuid) + '/' + str(zone.id) + '/'

        # check extension
        ext = file_obj.name.split('.')[-1]
        if ext.lower() not in ['png', 'jpg', 'gif']:
            return Response('Invalid image format.', status=status.HTTP_400_BAD_REQUEST)

        try:
            fs = FileSystemStorage()
            filename = fs.save(folder + file_obj.name.replace(' ', ''), file_obj)
            uploaded_file_url = fs.url(filename)

            # remove exiting
            if zone.screenshot:
                os.remove(os.path.join(settings.MEDIA_ROOT, zone.screenshot.replace(settings.MEDIA_URL, '')))

            zone.screenshot = uploaded_file_url
            zone.save()

        except Exception as e:
            logger.error(e)
            return Response([e.message], status=status.HTTP_400_BAD_REQUEST)

        return Response(uploaded_file_url, status=status.HTTP_201_CREATED)


class ZoneScreenshotRemoveView(APIView):
    def delete(self, request, pk, format=None):

        # check access
        if not request.user.is_staff and not request.user.is_authenticated:
            return Response(['You don\'t have permission to perform this action'], status=status.HTTP_403_FORBIDDEN)

        zone = get_object_or_404(Zone, pk=pk)
        folder = str(zone.project.uuid) + '/' + str(zone.id) + '/'

        if zone.screenshot:
            os.remove(os.path.join(settings.MEDIA_ROOT, zone.screenshot.replace(settings.MEDIA_URL, '')))

        zone.screenshot = None
        zone.save()

        return Response('OK', status=status.HTTP_204_NO_CONTENT)
