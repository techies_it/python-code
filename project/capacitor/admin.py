from django.contrib import admin

from .models import Script, Step, StepItem, Instruction, Note, Question, QuestionOption, Event, Zone, Navigation, Task

admin.site.register(Script)
admin.site.register(Step)
admin.site.register(StepItem)
admin.site.register(Instruction)
admin.site.register(Note)
admin.site.register(Question)
admin.site.register(QuestionOption)
admin.site.register(Event)
admin.site.register(Zone)
admin.site.register(Navigation)
admin.site.register(Task)
