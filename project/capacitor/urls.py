from django.conf.urls import url

from .views import QuestionResultAjaxUpdateView, QuestionOptionResultAjaxUpdateView
from .views import ScriptDownloadView, ScriptAjaxAddView, ScriptAjaxLoadView, ScriptAjaxUpdateView
from .views import ZoneScreenshotUploadView, ZoneScreenshotRemoveView

urlpatterns = [
    url(r'^project/(?P<id>.*)/script/download/(?P<sid>.*)', ScriptDownloadView.as_view(),
        name='project-script-download'),
    url('^scripts/ajax/add$', ScriptAjaxAddView.as_view(), name='script-add'),
    url('^scripts/ajax/(?P<id>.*)/load$', ScriptAjaxLoadView.as_view(), name='script-load'),
    url('^scripts/ajax/(?P<id>.*)/edit$', ScriptAjaxUpdateView.as_view(), name='script-update'),
    url('^question-result/ajax/edit$', QuestionResultAjaxUpdateView.as_view(), name='questionresult-update'),
    url('^question-option-result/ajax/edit$', QuestionOptionResultAjaxUpdateView.as_view(),
        name='questionoptionresult-update'),
    url('^zone/screenshot-upload$', ZoneScreenshotUploadView.as_view(), name='screenshot-upload'),
    url('^zone/screenshot-remove/(?P<pk>[0-9]+)/$', ZoneScreenshotRemoveView.as_view(), name='screenshot-remove'),
]
