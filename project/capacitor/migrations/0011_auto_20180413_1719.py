# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-04-13 07:19
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('capacitor', '0010_auto_20180321_1924'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='child_questions', to='capacitor.Question'),
        ),
        migrations.AddField(
            model_name='questionoption',
            name='value',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='stepitem',
            name='child_order',
            field=models.IntegerField(default=0),
        ),
    ]
