# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-02-21 02:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('capacitor', '0005_auto_20161215_1401'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='option_type',
            field=models.TextField(null=True),
        ),
    ]
