# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-02-27 04:00
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('recruit', '0006_auto_20170225_0001'),
        ('capacitor', '0007_auto_20170225_0001'),
    ]

    operations = [
        migrations.CreateModel(
            name='ScriptProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('profile', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='recruit.TestSubjectProfile')),
            ],
            options={
                'db_table': 'capacitor_script_profile',
            },
        ),
        migrations.AddField(
            model_name='scriptprofile',
            name='script',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='capacitor.Script'),
        ),
    ]
