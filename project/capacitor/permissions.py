from rest_framework import permissions

from .models import Zone, Event, Navigation


class IsAccountOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, user):
        if request.user:
            return user == request.user
        return False


class IsSameAccount(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user:
            if request.user.is_staff:
                return True
            if isinstance(obj, Zone) or \
                    isinstance(obj, Event) or \
                    isinstance(obj, Navigation):
                account = obj.project.interface.client.account
                return account in request.user.accounts.all()

        return False
