from django.db import models

from project.account.models import Project


class Event(models.Model):
    project = models.ForeignKey(Project, related_name='events')
    name = models.CharField(max_length=255)
    order = models.IntegerField()

    class Meta:
        unique_together = ('project', 'name',)

    def __str__(self):
        return '%s' % (self.name)

    def save(self, *args, **kwargs):
        super(Event, self).save(*args, **kwargs)
        self.project.save()

    def delete(self, *args, **kwargs):
        super(Event, self).delete(*args, **kwargs)
        self.project.save()


class Zone(models.Model):
    project = models.ForeignKey(Project, related_name='zones')
    name = models.CharField(max_length=255)
    screenshot = models.CharField(max_length=255, null=True, blank=True)
    order = models.IntegerField()

    class Meta:
        ordering = ('order',)
        unique_together = ('project', 'name',)

    def __str__(self):
        return '%s' % (self.name)

    def save(self, *args, **kwargs):
        super(Zone, self).save(*args, **kwargs)  # Call the "real" save() method.
        self.project.save()

    def delete(self, *args, **kwargs):
        # reset issues of this zone to empty zone
        if self.issues.count():
            # get first available zone
            for zone in self.project.zones.order_by('order'):
                if zone != self:
                    break
            for issues in self.issues.all():
                issues.zone = zone
                issues.save()

        super(Zone, self).delete(*args, **kwargs)
        self.project.save()


class Navigation(models.Model):
    project = models.ForeignKey(Project, related_name='navigation')
    name = models.CharField(max_length=255)
    order = models.IntegerField()

    class Meta:
        unique_together = ('project', 'name',)

    def __str__(self):
        return '%s' % (self.name)

    def save(self, *args, **kwargs):
        super(Navigation, self).save(*args, **kwargs)  # Call the "real" save() method.
        self.project.save()

    def delete(self, *args, **kwargs):
        super(Navigation, self).delete(*args, **kwargs)
        self.project.save()


class Script(models.Model):
    from project.recruit.models import TestSubjectProfile

    project = models.ForeignKey(Project, related_name='scripts')
    name = models.CharField(max_length=255)
    scenario = models.TextField(null=True)
    default_profiles = models.ManyToManyField(TestSubjectProfile, through='ScriptProfile', related_name='scripts',
                                              symmetrical=False)

    class Meta:
        ordering = ('id',)

    def __str__(self):
        return '%s' % (self.name)

    def save(self, *args, **kwargs):
        super(Script, self).save(*args, **kwargs)  # Call the "real" save() method.
        self.project.save()

    def delete(self, *args, **kwargs):
        # reset session of associate to this script
        if self.sessions.count():
            for session in self.sessions.all():
                session.script = None
                session.save()

        super(Script, self).delete(*args, **kwargs)
        self.project.save()


class ScriptProfile(models.Model):
    from project.recruit.models import TestSubjectProfile

    class Meta:
        db_table = 'capacitor_script_profile'
        # or whatever your table is called

    script = models.ForeignKey(Script)
    profile = models.ForeignKey(TestSubjectProfile)


class Step(models.Model):
    script = models.ForeignKey(Script, related_name='steps')
    default_zone = models.ForeignKey(Zone, related_name='steps', null=True, blank=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=255)
    order = models.IntegerField()

    class Meta:
        ordering = ('order',)

    def __str__(self):
        return '%s' % (self.name)


class StepItem(models.Model):
    step = models.ForeignKey(Step, related_name='items')
    order = models.IntegerField()
    child_order = models.IntegerField(default=0)

    class Meta:
        ordering = ('order',)

    def __str__(self):
        if hasattr(self, 'instruction'):
            return '%s %s %s' % (self.step.name, 'instruction', self.order)
        if hasattr(self, 'note'):
            return '%s %s %s' % (self.step.name, 'note', self.order)
        if hasattr(self, 'task'):
            return '%s %s %s' % (self.step.name, 'task', self.order)
        if hasattr(self, 'question'):
            return '%s %s %s' % (self.step.name, 'question', self.order)
        if hasattr(self, 'instruction'):
            return '%s %s' % (self.step.name, self.order)

    def type_name(self):
        if hasattr(self, 'instruction'):
            return 'instruction'
        if hasattr(self, 'note'):
            return 'note'
        if hasattr(self, 'task'):
            return 'task'
        if hasattr(self, 'question'):
            return 'question'
        return None


class Instruction(models.Model):
    step_item = models.OneToOneField(StepItem, on_delete=models.CASCADE, primary_key=True)
    content = models.TextField()


class Note(models.Model):
    step_item = models.OneToOneField(StepItem, on_delete=models.CASCADE, primary_key=True)
    content = models.TextField()


class Task(models.Model):
    step_item = models.OneToOneField(StepItem, on_delete=models.CASCADE, primary_key=True)
    content = models.CharField(max_length=255)

    def getResultBySession(self, session_id):
        from project.report.models import TaskResult
        return TaskResult.objects.filter(task=self, session__id=session_id).first()

    @property
    def getResultByAllSession(self):
        from project.report.models import TaskResult
        return TaskResult.objects.filter(task=self).all()

    def getResultByAllSessionFiltered(self, profile):
        """
        This function is used to filter the results by profile name.
        This function is used for filters at progress page responses tab at frontend.
        """
        from project.report.models import TaskResult
        return TaskResult.objects.filter(task=self, session__test_subject_profile__name=profile).all()    

    @property
    def getResultAverageSuccess(self):
        count = 0
        total = 0
        total_time = 0
        for result in self.getResultByAllSession:
            if result and result.session.display_name and result.elapsed_time_formatted and result.status:
                total = total + 1
                if result.status == 'success':
                    count = count + 1
                    total_time += result.used_time.seconds
        if count:
            m, s = divmod(round(total_time / count), 60)
            h, m = divmod(m, 60)
            average = "%02d:%02d:%02d" % (h, m, s)
            return str(count) + '(' + str(count / total * 100) + '%) ' + \
                   'Average time: ' + average
        return 'None'

    @property
    def getResultAverageFailed(self):
        count = 0
        total = 0
        total_time = 0
        for result in self.getResultByAllSession:
            if result and result.session.display_name and result.elapsed_time_formatted and result.status:
                total = total + 1
                if result.status == 'fail':
                    count = count + 1
                    total_time += result.used_time.seconds
        if count:
            m, s = divmod(round(total_time / count), 60)
            h, m = divmod(m, 60)
            average = "%02d:%02d:%02d" % (h, m, s)
            return str(count) + '(' + str(count / total * 100) + '%) ' + \
                   'Average time: ' + average
        return 'None'

    @property
    def getResultAverageUnclear(self):
        count = 0
        total = 0
        total_time = 0
        for result in self.getResultByAllSession:
            if result and result.session.display_name and result.elapsed_time_formatted and result.status:
                total = total + 1
                if result.status == 'unclear':
                    count = count + 1
                    total_time += result.used_time.seconds
        if count:
            m, s = divmod(round(total_time / count), 60)
            h, m = divmod(m, 60)
            average = "%02d:%02d:%02d" % (h, m, s)
            return str(count) + '(' + str(count / total * 100) + '%) ' + \
                   'Average time: ' + average
        return 'None'


class Question(models.Model):
    step_item = models.OneToOneField(StepItem, on_delete=models.CASCADE, primary_key=True)
    name = models.TextField()
    note = models.TextField(null=True)
    is_multi = models.BooleanField(default=False, blank=True)
    option_type = models.TextField(null=True)
    parent = models.ForeignKey("self", null=True, blank=True, related_name='child_questions')

    def __str__(self):
        return '%s' % (self.name)

    def getResultBySession(self, session_id):
        from project.report.models import QuestionResult
        return QuestionResult.objects.filter(question=self, session__id=session_id).first()

    def getResultByAllSessionFiltered(self, profile):
        """
        This function is used to filter the results by profile name.
        This function is used for filters at progress page responses tab at frontend.
        """
        from project.report.models import QuestionResult
        return QuestionResult.objects.filter(question=self, session__test_subject_profile__name=profile).all()    

    @property
    def getResultByAllSession(self):
        from project.report.models import QuestionResult
        return QuestionResult.objects.filter(question=self).all()

    @property
    def getResultAverage(self):
        if self.option_type == '1to10' or self.option_type == '1to5':
            total = 0
            count = 0
            for result in self.getResultByAllSession:
                if result and result.session.display_name and result.response:
                    try:
                        total += float(result.response)
                        count = count + 1
                    except ValueError:
                        pass  # do nothing!
            if count:
                return "{0:0.1f}".format(total / count)
        return None

    @property
    def is_parent(self):
        if self.option_type != 'sus':
            return True
        return self.child_questions.first() and True or False      

    def getNpsSusScore(self, profile_name=None):
        score_detail = {'score': None}
        if self.option_type == 'nps':
            total_respondents = 0
            total_detractors = 0
            total_promoters = 0
            if profile_name:
                results = self.getResultByAllSessionFiltered(profile_name)
            else:
                results = self.getResultByAllSession    
            for result in results:
                if result and result.session.display_name and result.response:
                    try:
                        score_value = int(result.response)
                        if score_value <= 6:
                            total_detractors = total_detractors + 1
                        elif score_value >= 9 and score_value <=10:
                            total_promoters = total_promoters + 1  
                        total_respondents = total_respondents + 1
                    except ValueError:
                        pass  # do nothing!
            if total_respondents:
                score = float(total_promoters - total_detractors)/(total_respondents) * 100
                total_passives = total_respondents - total_detractors - total_promoters
                if score < 0:
                    progress_bar_nps = (score + 100)/2
                else:
                    progress_bar_nps = (score/2)  + 50    

                score = "{0:0.1f}".format(score)    
                score_detail = {'total_detractors': total_detractors, 'total_promoters': total_promoters,
                    'total_passives': total_passives, 'total_respondents': total_respondents,
                    'score': score, 'progress_bar_nps':progress_bar_nps}
                return score_detail
        
        # sus score calculation
        if self.option_type == 'sus' and self.is_parent:
            sum_sus_score = 0
            total_respondents = 0
            total_sessions = {}
            for sus_obj in self.child_questions.order_by('step_item__child_order'):
                if profile_name:
                    results = sus_obj.getResultByAllSessionFiltered(profile_name)
                else:
                    results = sus_obj.getResultByAllSession
                for result in results:
                    if result and result.session.display_name and result.response:
                        try:
                            total_sessions[result.session_id] = True
                            if result.response == "Strongly disagree":
                                response = 1
                            elif result.response == "Disagree":
                                response = 2
                            elif result.response == "Neither":
                                response = 3
                            elif result.response == "Neither disagree or agree":
                                response = 3
                            elif result.response == "Agree":
                                response = 4
                            elif result.response == "Strongly agree":
                                response = 5
                            else:
                                response = result.response                     
                            scale_value = int(response)
                            if sus_obj.step_item.child_order % 2 == 1:
                                score_value = scale_value - 1
                            else:
                                score_value = 5 - scale_value
                            sum_sus_score = sum_sus_score + score_value
                        except ValueError:
                            pass  # do nothing!
            total_respondents = len(total_sessions)
            sum_sus_score = sum_sus_score*2.5
            if total_respondents:
                score = "{0:0.1f}".format(sum_sus_score / total_respondents)
                score_detail = {'sum_sus_score': sum_sus_score, 'total_respondents': total_respondents, 'score': score}
                return score_detail

        return score_detail

    @property
    def sorted_child_questions(self):
        return self.child_questions.order_by('step_item__child_order')

class QuestionOption(models.Model):
    question = models.ForeignKey(Question, related_name='question_options')
    name = models.CharField(max_length=255)
    value = models.IntegerField(null=True, blank=True)
    order = models.IntegerField()

    class Meta:
        ordering = ('order',)

    def __str__(self):
        return '%s' % (self.name)

    def getResultBySession(self, session_id):
        from project.report.models import QuestionOptionResult
        return QuestionOptionResult.objects.filter(option=self, session__id=session_id).first()

    def getResultByAllSessionFiltered(self, profile):
        """
        This function is used to filter the results by profile name.
        This function is used for filters at progress page responses tab at frontend.
        """
        from project.report.models import QuestionOptionResult
        return QuestionOptionResult.objects.filter(option=self, session__test_subject_profile__name=profile).all()    

    @property
    def getResultByAllSession(self):
        from project.report.models import QuestionOptionResult
        return QuestionOptionResult.objects.filter(option=self).all()
