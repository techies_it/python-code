from rest_framework import serializers

from project.account.serializers import ProjectSerializer
from .models import Script, Step, Question, QuestionOption, Event, Zone, Navigation, Task


class ZoneSerializer(serializers.ModelSerializer):
    # project = ProjectSerializer(required=False)
    # steps = serializers.HyperlinkedIdentityField(view_name='zonestep-list')

    class Meta:
        model = Zone


class EventSerializer(serializers.ModelSerializer):
    # project = ProjectSerializer(required=False)

    class Meta:
        model = Event


class NavigationSerializer(serializers.ModelSerializer):
    # project = ProjectSerializer(required=False)

    class Meta:
        model = Navigation


class TaskSerializer(serializers.ModelSerializer):
    # project = ProjectSerializer(required=False)

    class Meta:
        model = Task


class ScriptSerializer(serializers.ModelSerializer):
    project = ProjectSerializer(required=False)
    steps = serializers.HyperlinkedIdentityField(view_name='scriptstep-list')

    class Meta:
        model = Script


class StepSerializer(serializers.ModelSerializer):
    script = ScriptSerializer(required=False)
    default_zone = ZoneSerializer(required=False)
    questions = serializers.HyperlinkedIdentityField(view_name='stepquestion-list')

    class Meta:
        model = Step


class QuestionSerializer(serializers.ModelSerializer):
    step = StepSerializer(required=False)
    question_options = serializers.HyperlinkedIdentityField(view_name='questionquestionoption-list')

    class Meta:
        model = Question


class QuestionOptionSerializer(serializers.ModelSerializer):
    question = QuestionSerializer(required=False)

    class Meta:
        model = QuestionOption
