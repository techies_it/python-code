import json
import logging
import re

from django.contrib.auth import authenticate, login, logout
from rest_framework import permissions, status, views
from rest_framework.response import Response

from project.account.models import User
from project.account.serializers import UserSerializer

logger = logging.getLogger(__name__)

email_re = re.compile(
    r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"
    r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*"'
    r')@(?:[A-Z0-9-]+\.)+[A-Z]{2,6}$', re.IGNORECASE)


class LoginView(views.APIView):
    def post(self, request, format=None):
        data = json.loads(request.body)

        email = data.get('email', None)
        logger.debug('In login, email: %s' % email)
        password = data.get('password', None)

        if email_re.search(email):
            try:
                user = User.objects.get(email__iexact=email)
            except User.DoesNotExist:
                logger.debug('Email not found')
                return Response({
                    'status': 'Unauthorized',
                    'message': 'Invalid username/password combination.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            try:
                user = User.objects.get(username=email)
            except User.DoesNotExist:
                logger.debug('Username not found')
                return Response({
                    'status': 'Unauthorized',
                    'message': 'Invalid username/password combination.'
                }, status=status.HTTP_401_UNAUTHORIZED)

        if user.check_password(password):
            if user.is_active:
                logger.debug('User is active, authenticating')
                user = authenticate(username=user.username, password=password)
                login(request, user)

                serialized = UserSerializer(user)

                response = Response(serialized.data)
                logger.debug('Login successful, returning %s' % serialized.data)
                return response
            else:
                logger.debug('User is not active')
                return Response({
                    'status': 'Unauthorized',
                    'message': 'This account has been disabled.'
                }, status=status.HTTP_401_UNAUTHORIZED)

        logger.debug('Invalid password')
        return Response({
            'status': 'Unauthorized',
            'message': 'Invalid username/password combination.'
        }, status=status.HTTP_401_UNAUTHORIZED)


class LogoutView(views.APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        logger.debug('In logout')
        logout(request)

        return Response({}, status=status.HTTP_204_NO_CONTENT)
