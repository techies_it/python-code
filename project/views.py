from django.conf import settings
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic.base import TemplateView


class IndexView(TemplateView):
    template_name = 'index.html'

    @method_decorator(ensure_csrf_cookie)
    def dispatch(self, *args, **kwargs):
        if self.request.path == '/' and self.request.user.is_authenticated():
            return redirect('/projects')
        return super(IndexView, self).dispatch(*args, **kwargs)

    def is_flux_recruit(self):
        return self.request.path.split('/')[-1] == 'recruits' and True or False

    def is_recording(self):
        url = self.request.path.split('/')

        if url[1] == 'project' and url[3] == 'session' and url[5] == 'script':
            return True;

        if url[1] == 'observer':
            return True;

        return False

    def env(self):
        return settings.ENVIRON

    def is_angular(self):
        return True
