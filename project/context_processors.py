# The context processor function
def get_user_nav(request):
    user = request.user
    if user.is_authenticated():
        user_nav = [
            {'href': '/profile/' + str(user.id) + '/settings', 'name': user.display_name},
        ]
    else:
        user_nav = []
    return {'user_nav': user_nav}


# The context processor function
def get_site_nav(request):
    user = request.user
    nav = []

    if not user.is_authenticated():
        link = {'href': '/', 'name': 'Home'}
        nav.append(link)
        link = {'href': '/features#list', 'name': 'Features'}
        nav.append(link)
        # link = {'href': '/accounts/register', 'name': 'Sign up'}
        # nav.append(link)
        link = {'href': '/support', 'name': 'Support'}
        nav.append(link)
        return {'site_nav': nav}

    if user.is_staff:
        link = {'href': '/accounts', 'name': 'Accounts'}
        nav.append(link)

    if user.is_staff or user.is_authenticated() and user.accounts.all():
        link = {'href': '/projects', 'name': 'Projects'}
        nav.append(link)

    if user.is_staff or user.is_authenticated() and user.accounts.all():
        link = {'href': '/recruiters', 'name': 'Recruiters'}
        nav.append(link)

    if user.is_authenticated() and user.accounts.all():
        link = {'href': '/users', 'name': 'Users'}
        nav.append(link)

    return {'site_nav': nav}
